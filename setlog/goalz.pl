consult('/home/jmesuro/ztosetlog/setlog/setlog.pl').
set_prolog_flag(toplevel_print_options, [quoted(true), portray(true)]).
use_module(library(dialect/sicstus/timeout)).
set_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)]).
time_out(setlog( 
NAT = int(0, 2147483649) & 
NAT1 = int(1, 2147483647) & 
INT = int(-2147483648, 2147483647) & 
labeling(X) & 
labeling(Y) & 
labeling(AUX0) & 
AUX0 is X + Y & 
0 < AUX0
,_CONSTR),1000,_RET).

