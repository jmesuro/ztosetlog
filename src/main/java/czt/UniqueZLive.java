package czt;

import net.sourceforge.czt.animation.eval.ZLive;

/**
 * Created by jmesuro on 06/04/16.
 */
public final class UniqueZLive{

    private static ZLive zLive = null;

    public static ZLive getInstance(){
        if(zLive==null)
            zLive = new ZLive();
        return zLive;
    }
}