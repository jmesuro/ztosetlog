package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.*;

/**
 * Created by joaquin on 06/10/16 ztosetlog.
 */
public class VarDeclExtractor implements
        VarDeclVisitor<Map<String,String>>,
        TermVisitor<Map<String,String>>
{

    private Map<String,String> map;

    public VarDeclExtractor(){
        map = new HashMap<String, String>();
    }

    public Map<String,String> visitVarDecl(VarDecl varDecl) {
        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            map.putAll(expr.accept(this));
            if (!map.containsKey(var.toString()))
                map.put(var.toString(), SpecUtils.termToLatex(expr));

        }
        return map;
    }

    public Map<String,String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm;
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;
                map.putAll(auxTerm.accept(this));
            }
        }
        return map;
    }
}