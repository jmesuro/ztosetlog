package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.base.visitor.VisitorUtils;



/**
 * An instance of this class allow the deep cloning of terms. This class is based on the 
 * Visitor design pattern.
 * @author Pablo Rodriguez Monetti
 */
public final class CZTCloner
	implements TermVisitor<Term>
{

/*	private void visitAnns(Term term, Term result){
    	List<Object> anns = result.getAnns();
    	for (Object o : term.getAnns()) {
    		if (o instanceof Term) {
        		anns.add(((Term) o).accept(this));
    		}
    		else {
        		anns.add(o);
    		}
    	}
  	}*/


	public Term visitTerm(Term term){
		//visitAnns(term, result);
    	return VisitorUtils.visitTerm(this, term, false);
	}



}