package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.AxParaVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by joaquin on 06/10/16 ztosetlog.
 */
public class VarDeclExtractorByOps implements
        TermVisitor<Term>,
        AxParaVisitor<Term>
{

    private Map<String, Map<String,String>> ops;
    private Spec spec;

    public Map<String, Map<String, String>> getOps() {
        return ops;
    }

    public VarDeclExtractorByOps(Spec spec){
        this.ops = new HashMap<String, Map<String,String>>();
        this.spec = spec;
    }


    public Term visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm = null;
        List<String> l = new ArrayList<String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;

                auxTerm.accept(this);
            }
        }
        return auxTerm;
    }


    public Term visitAxPara(AxPara axPara) {

        if (axPara.getBox().toString().equals("OmitBox") || axPara.getBox().toString().equals("SchBox")){
            String name = SpecUtils.getAxParaName(axPara);
            VarDeclExtractorRecursive varDeclExtractorRecursive = new VarDeclExtractorRecursive(spec);
            Map m = axPara.accept(varDeclExtractorRecursive);
            ops.put(name,m);
        }

        return axPara;


//        ZSchText zSchText = axPara.getZSchText();
//        DeclList declList = zSchText.getDeclList();
//        String schemeName = "";
//        SchemeType schemeType = null;
//        if (declList instanceof ZDeclList) {
//            ZDeclList zDeclList = (ZDeclList) declList;
//            for (Decl decl : zDeclList) {
//                if (decl instanceof ConstDecl) {
//                    ConstDecl constDecl = (ConstDecl) decl;
//                    ZName zName = constDecl.getZName();
//                    schemeName = zName.getWord();
//                    // If the type of this scheme was previously calculated
//                    // it is returned
//                    schemeType = schemesTypes.get(schemeName);
//                    if (schemeType != null) {
//                        return schemeType;
//                    }
//                    Expr expr = constDecl.getExpr();
//                    schemeType = expr.accept(this);
//                }
//            }
//        }
//        schemesTypes.put(schemeName, schemeType);
//
//
//        if (schemeType == SchemeType.OP)
//            opNames.add(schemeName);
//        return schemeType;
    }
}