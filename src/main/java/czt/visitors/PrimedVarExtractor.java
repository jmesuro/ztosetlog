package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaquin on 02/04/17 z2setlog.
 */
public class PrimedVarExtractor implements
        TermVisitor<List<String>>,
        RefExprVisitor<List<String>>,
        AxParaVisitor<List<String>>,
        AndPredVisitor<List<String>>,
        MemPredVisitor<List<String>> {



    public List<String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        List<String> primedVars = new ArrayList<String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                primedVars.addAll(((Term) object).accept(this));
            }
        }
        return primedVars;
    }

    public List<String> visitRefExpr(RefExpr refExpr) {
        List<String> l = new ArrayList<String>();
        String sname = refExpr.getZName().toString();

        if (sname.endsWith("′"))
            l.add(sname);

        return l;
    }

    public List<String> visitAxPara(AxPara axPara) {
        ZSchText zSchText = axPara.getZSchText();
        ZDeclList zDeclList = zSchText.getZDeclList();
        ConstDecl constDecl = (ConstDecl) zDeclList.get(0);
        SchExpr schExpr = (SchExpr) constDecl.getExpr();
        ZSchText zSchText1 = schExpr.getZSchText();
        Pred pred = zSchText1.getPred();
        return pred.accept(this);
    }

    public List<String> visitAndPred(AndPred andPred) {
        List<String> primedVars = new ArrayList<String>();
        primedVars.addAll(andPred.getLeftPred().accept(this));
        primedVars.addAll(andPred.getRightPred().accept(this));
        return primedVars;
    }

    public List<String> visitMemPred(MemPred memPred) {
        List<String> primedVars = new ArrayList<String>();
        primedVars.addAll(memPred.getLeftExpr().accept(this));
        return primedVars;
    }
}