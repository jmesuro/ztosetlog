package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 27/10/16.
 */
public final class VarCZTDeclExtractor implements
        VarDeclVisitor<Map<String,Expr>>,
        TermVisitor<Map<String,Expr>> {

    public Map<String,Expr> visitVarDecl(VarDecl varDecl) {

        Map<String,Expr> map = new HashMap<String,Expr>();
        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            map.putAll(expr.accept(this));
            map.put(var.toString(), expr);
        }
        return map;
    }

    public Map<String,Expr> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Term auxTerm;
        Map<String,Expr> map = new HashMap<String,Expr>();
        for (final Object object : array) {
            if (object instanceof Term) {
                auxTerm = (Term) object;
                map.putAll(auxTerm.accept(this));
            }
        }
        return map;
    }

}