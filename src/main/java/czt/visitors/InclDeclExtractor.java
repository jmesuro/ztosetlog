package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.InclDecl;
import net.sourceforge.czt.z.visitor.InclDeclVisitor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaquin on 02/04/17 z2setlog.
 */
public class InclDeclExtractor implements InclDeclVisitor<List<String>>,
                                          TermVisitor<List<String>> {
    public List<String> visitInclDecl(InclDecl inclDecl) {
        List<String> inclDecls = new ArrayList<String>();
        inclDecls.add(SpecUtils.termToLatex(inclDecl.getExpr()));
        return inclDecls;
    }


    public List<String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        List<String> inclDecls = new ArrayList<String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                inclDecls.addAll(((Term) object).accept(this));
            }
        }
        return inclDecls;
    }
}
