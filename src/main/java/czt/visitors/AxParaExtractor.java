package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.AxParaVisitor;

import java.util.*;

/**
 * Created by joaquin on 02/04/17 z2setlog.
 */
public class AxParaExtractor implements TermVisitor<Map<String, AxPara>>,
                                        AxParaVisitor<Map<String, AxPara>> {

    private Collection<String> colNames;

    public AxParaExtractor(Collection<String> colNames){
        this.colNames = colNames;
    }

    public Map<String, AxPara> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Map<String, AxPara> map = new HashMap<String, AxPara>();
        for (final Object object : array) {
            if (object instanceof Term) {
                map.putAll(((Term) object).accept(this));
            }
        }
        return map;
    }

    public Map<String, AxPara> visitAxPara(AxPara axPara) {
        Map<String, AxPara> map = new HashMap<String, AxPara>();
        ZDeclList zDeclList = axPara.getZSchText().getZDeclList();
        for (Decl decl : zDeclList.getDecl()){
            if (decl instanceof ConstDecl){
                String name = ((ConstDecl)decl).getZName().toString();
                if (colNames.contains(name))
                    map.put(name,axPara);
            }

        }
        return map;
    }
}
