package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.ConstDeclVisitor;
import java.util.*;

/**
 * Created by joaquin on 19/06/16.
 */
public final class ConstDeclExtractor implements
        TermVisitor<Map<String,Expr>>,
        ConstDeclVisitor<Map<String,Expr>>{

   public Map<String, Expr> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Map<String, Expr> s = new HashMap<String, Expr>();
        for (final Object object : array) {
            if (object instanceof Term) {
                s.putAll(((Term) object).accept(this));
            }
        }
        return s;
    }

    public Map<String, Expr> visitConstDecl(ConstDecl constDecl) {
        Map<String,Expr> s = new HashMap<String, Expr>();
        s.put(constDecl.getZName().toString(),constDecl.getExpr());
        return s;
    }
}