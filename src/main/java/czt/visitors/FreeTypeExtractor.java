/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.FreeParaVisitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jmesuro on 07/05/16.
 */
public final class FreeTypeExtractor implements
        FreeParaVisitor<Map<String, List<String>>>,
        TermVisitor<Map<String, List<String>>> {


    public Map<String, List<String>> visitFreePara(FreePara freePara) {
        Map<String, List<String>> freeTypes = new HashMap<String, List<String>>();
        String name = ((ZFreetypeList) freePara.getFreetypeList()).get(0).getZName().toString();
        List<String> elems = new ArrayList<String>();

        for (Branch branch : ((ZBranchList) ((ZFreetypeList) freePara
                .getFreetypeList()).get(0).getBranchList())) {
            elems.add(branch.getZName().toString());
        }
        freeTypes.put(name, elems);
        return freeTypes;
    }

    public Map<String, List<String>> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Map<String, List<String>> freeType = new HashMap<String, List<String>>();
        for (final Object object : array) {
            if (object instanceof Term) {
                freeType.putAll(((Term) object).accept(this));
            }
        }
        return freeType;
    }

}
