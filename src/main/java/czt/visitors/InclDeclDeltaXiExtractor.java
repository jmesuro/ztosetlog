package czt.visitors;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.InclDecl;
import net.sourceforge.czt.z.ast.RefExpr;
import net.sourceforge.czt.z.visitor.InclDeclVisitor;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by joaquin on 02/04/17 z2setlog.
 * Extrae un inclDecl que esta incluido como xi o delta, pero
 */
public class InclDeclDeltaXiExtractor implements InclDeclVisitor<Set<String>>,
                                          TermVisitor<Set<String>> {



    public Set<String> visitInclDecl(InclDecl inclDecl) {
        Set<String> inclDecls = new HashSet<String>();

        Expr expr = inclDecl.getExpr();
        if (expr instanceof RefExpr) {
            String refExprName = ((RefExpr) expr).getName().toString();
            if (((RefExpr)inclDecl.getExpr()).getZName().getId().equals("deltaxi") )
                inclDecls.add("init" + refExprName.substring(1,refExprName.length()));
        }
        return inclDecls;
    }


    public Set<String> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Set<String> inclDecls = new HashSet<String>();
        for (final Object object : array) {
            if (object instanceof Term) {
                inclDecls.addAll(((Term) object).accept(this));
            }
        }
        return inclDecls;
    }
}
