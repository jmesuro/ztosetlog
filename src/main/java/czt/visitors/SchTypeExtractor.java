package czt.visitors;

import czt.SpecUtils;
import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 03/11/16.
 * Por si algún día quiero emprolijar el hecho de que se use SchType como Cross
 * Entonces debería tener este extractor. como lo hago con FreeType.
 */
public final class SchTypeExtractor implements
                TermVisitor<Map<String, Map<String,String>>>,
                VarDeclVisitor<Map<String,Map<String,String>>>{


    private Map<String, Expr> constDecl;

    public SchTypeExtractor(Map<String, Expr> constDecl){
        this.constDecl = constDecl;
    }

    public Map<String, Map<String,String>> visitTerm(Term term) {
        Object[] array = term.getChildren();
        Map<String, Map<String,String>> schType = new HashMap<String, Map<String,String>>();
        for (final Object object : array) {
            if (object instanceof Term) {
                schType.putAll(((Term) object).accept(this));
            }
        }
        return schType;
    }

    private Map<String,String> varDeclNoDeep(VarDecl varDecl){
        Map<String,String> map = new HashMap<String,String>();
        for (Name var : varDecl.getZNameList())
            map.put(var.toString(), SpecUtils.termToLatex(varDecl.getExpr()) );

        return map;
    }

    private Map<String,String> schDeclVars(SchExpr schExpr){
        ZSchText zSchText = schExpr.getZSchText();
        Map<String,String> ivars = new HashMap<String, String>();

        for (Decl decli : zSchText.getZDeclList())
            ivars.putAll( varDeclNoDeep((VarDecl) decli));
        return ivars;
    }

    public Map<String, Map<String,String>> visitVarDecl(VarDecl varDecl) {
        Map<String,Map<String,String>> map = new HashMap<String,Map<String,String>>();

        for (Name var : varDecl.getZNameList()) {
            Expr expr = varDecl.getExpr();
            String exprStr = SpecUtils.termToLatex(expr);

            //map.putAll(expr.accept(this));

//            if (expr instanceof SchExpr){
////                map.put(var.toString(),schDeclVars((SchExpr) expr));
////                map.putAll(expr.accept(this));
//            }
//            else{
////                String exprStr = SpecUtils.termToLatex(expr);
////                if (constDecl.containsKey( exprStr)){
////                    Expr expr2 = constDecl.get(exprStr);
////                    if ( expr2 instanceof SchExpr){
////                        map.put(var.toString(),schDeclVars((SchExpr) expr2));
////                        map.putAll(expr2.accept(this));
////                    }
//                }

                    for (String cDKey : constDecl.keySet() ){
                        if (exprStr.contains(cDKey)){
                            Expr expr2 = constDecl.get(cDKey);
                            if ( expr2 instanceof SchExpr){
                                map.put(cDKey,schDeclVars((SchExpr) expr2));
                                map.putAll(expr2.accept(this));
                            }
                        }
                    }
            }


        return map;
    }




}
