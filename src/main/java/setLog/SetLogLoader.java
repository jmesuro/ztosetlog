package setLog;

import java.io.*;

/**
 * Created by jmesuro on 01/08/16.
 */
public final class SetLogLoader {

    private String setLogConfFileName;


    public SetLogLoader(String setLogConfFileName) {
        this.setLogConfFileName = setLogConfFileName;
    }

    //from ztosetlog
    public void loadconf(){
        System.out.print("Loading setlog.conf... ");

        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(setLogConfFileName)));
            String line;
            String setLogFile = null,setLogLib = null, tout = null;
            if ((line = in.readLine())!= null)
                setLogFile = line.split(" ")[1];
            if ((line = in.readLine())!= null)
                tout = line.split(" ")[1];
            if ((line = in.readLine())!= null)
                setLogLib = line.split(" ")[1];
            in.close();
            SetLogIO.getInstance().setConf(setLogFile, tout);

            System.out.println("setlog loaded " + setLogFile.substring(0,setLogFile.length()-3));




        } catch (FileNotFoundException e) {
            System.out.println("The 'setlog.conf' configuration file " +
                    "could not be found.");
            System.exit(0);
        } catch (IOException e) {
            System.out.println("IOException");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Couldn't find setlog.conf.");
            e.printStackTrace();
            System.exit(0);
        }
    }

    //from FASTEST
    public void loadconf(String setlogpath) throws Exception{


        System.out.print("Loading setlog.conf... ");

        String line;
        String setLogFile = null,setLogLib = null, tout = null;
        try {
            BufferedReader in = new BufferedReader(new FileReader(new File(setLogConfFileName)));


            if ((line = in.readLine())!= null)
                setLogFile = line.split(" ")[1];
            if ((line = in.readLine())!= null)
                tout = line.split(" ")[1];
            if ((line = in.readLine())!= null)
                setLogLib = line.split(" ")[1];
            in.close();
        }

        catch (FileNotFoundException e) {
            System.out.println("The 'setlog.conf' configuration file " +
                    "could not be found.");
            System.exit(0);
        } catch (IOException e) {
            System.out.println("IOException");
            System.exit(0);
        } catch (Exception e) {
            System.out.println("Couldn't find setlog.conf.");
            e.printStackTrace();
            System.exit(0);
        }

        String path_prueba = setlogpath + setLogFile;
        new BufferedReader(new FileReader(new File(path_prueba)));

        SetLogIO.getInstance().setConf(setLogFile,tout,setlogpath);

        System.out.println("setlog loaded " + setLogFile.substring(0,setLogFile.length()-3));

    }

}
