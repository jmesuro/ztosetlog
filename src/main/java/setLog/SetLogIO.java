package setLog;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmesuro on 28/07/16.
 */
public final class SetLogIO{
    private String setLogFile;
    private int tout;
    private String path = System.getProperty("user.dir") + "/setlog/";
    private String setLogLib;
    private static int contador = 0;

    private static SetLogIO ourInstance;

    public static SetLogIO getInstance() {
        if (ourInstance == null)
            ourInstance = new SetLogIO();

        return ourInstance;
    }

    private SetLogIO(){
    }

    void setConf(String setLogFile, String tout, String path){
        this.setLogFile = setLogFile;
        this.tout = Integer.parseInt(tout);
        this.path = path;
        // this.setLogLib = setLogLib;

    }

    void setConf(String setLogFile, String tout){
        this.setLogFile = setLogFile;
        this.tout = Integer.parseInt(tout);
        //this.setLogLib = setLogLib;

    }

    public String runSetLog(String setlogInput, StringBuilder setlogOutput){
        String sOutErr = "OK";
        String sout = "";
        StringBuilder setlogOutputAux = new StringBuilder();



        //System.out.println("****************************************** Inicia prolog ******** ");
        try{
            // prepare prolog execution process
            ProcessBuilder builder = new ProcessBuilder();
            builder.command("prolog" , "-q");

            builder.directory(new File(System.getProperty("user.dir")));
            Process proc = builder.start();
            int exitCode = 0;

            OutputStream out = proc.getOutputStream();

            path = URLDecoder.decode(path, "UTF-8");

            // build prolog query to invoke setlog on the input
            String goal = "consult('" + path + setLogFile + "')."
                    + "\nset_prolog_flag(toplevel_print_options, [quoted(true), portray(true)])."
                    + "\nuse_module(library(dialect/sicstus/timeout))."
                    //+ "\nconsult('" + path + setLogLib + "')."
                    + "\nset_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)])."
                    + "\ntime_out(setlog( \n"
                    + setlogInput
                    + "\n,_CONSTR)," + tout + ",_RET).\n\n";

            out.write(goal.getBytes());
            out.close();

            exitCode = proc.waitFor();

            boolean pruneClass = false;
            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            //System.out.println("********************************************* STDERROR  ******** ");
            String line;

            boolean err = false;
            while ((line = stdError.readLine()) != null) {
                //System.out.println("********************************************* LINEA ******** ");

                if (!err) {
                    if (line.equals("false.")) {
                        sOutErr = "FALSE";
                        err = true;
                    } else if (line.equals("_RET = time_out.")) {
                        sOutErr = "TIMEOUT";
                        err = true;
                    } else if (line.startsWith("ERROR:")) {
                        sOutErr = "ERROR";
                        err = true;
                    } else if (line.startsWith("***WARNING***")) {
                        sOutErr = "WARNING";
                        err = true;
                    }
                }

                if (err){
                    do
                        setlogOutputAux.append(line).append("\n");
                    while((line = stdError.readLine()) != null);
                    sout = setlogOutputAux.toString();
                    break;
                }

                if ((!line.equals("")) && (!line.startsWith("true.")) && (!line.startsWith("_CONSTR")) && (!line.startsWith("Use ?")))
                    setlogOutputAux.append(line).append("\n");


                else if(line.startsWith("_CONSTR"))
                    sout = line +"\n"+ setlogOutputAux;


            }
        }

//    public String runSetLog(String setlogInput, StringBuilder setlogOutput){
//        String sOutErr = "OK";
//        String sout = "";
//        StringBuilder setlogOutputAux = new StringBuilder();
//
//
//
//        System.out.println("****************************************** Inicia prolog ******** ");
//        try{
//            // prepare prolog execution process
//            String[] cmd = {"prolog" , "-q"};
//            final Process proc = Runtime.getRuntime().exec(cmd);
//            OutputStream out = proc.getOutputStream();
//
//            // build setlog path
//            //String path = System.getProperty("user.dir") + "/src/main/resources/setlog/";
//            //String path = System.getProperty("user.dir") + "/setlog/";
//
//            path = URLDecoder.decode(path, "UTF-8");
//
//            // build prolog query to invoke setlog on the input
//            String goal = "consult('" + path + setLogFile + "')."
//                    + "\nset_prolog_flag(toplevel_print_options, [quoted(true), portray(true)])."
//                    + "\nuse_module(library(dialect/sicstus/timeout))."
//                     //"\nconsult('" + path + setLogLib + "')."
//                    + "\nset_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)])."
//                    + "\ntime_out(setlog( \n"
//                    + setlogInput
//                    + "\n,_CONSTR)," + tout + ",_RET).\n\n";
//
//            out.write(goal.getBytes());
//            out.close();
//
//            System.out.println( goal);
//            PrintWriter writer = new PrintWriter(String.valueOf(contador++) + "_goal.txt", "UTF-8");
//            writer.print(goal);
//            writer.close();
//
//            boolean pruneClass = false;
//            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//            System.out.println("********************************************* STDERROR  ******** ");
//            //BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
//            String line;
//
//            boolean err = false;
//            while ((line = stdError.readLine()) != null) {
//                System.out.println("********************************************* LINEA ******** ");
//
//                if (!err) {
//                    if (line.equals("false.")) {
//                        sOutErr = "FALSE";
//                        err = true;
//                    } else if (line.equals("_RET = time_out.")) {
//                        sOutErr = "TIMEOUT";
//                        err = true;
//                    } else if (line.startsWith("ERROR:")) {
//                        sOutErr = "ERROR";
//                        err = true;
//                    } else if (line.startsWith("***WARNING***")) {
//                        sOutErr = "WARNING";
//                        err = true;
//                    }
//                }
//
//                if (err){
//                    do
//                        setlogOutputAux.append(line).append("\n");
//                    while((line = stdError.readLine()) != null);
//                    sout = setlogOutputAux.toString();
//                    break;
//                }
//
//                if ((!line.equals("")) && (!line.startsWith("true.")) && (!line.startsWith("_CONSTR")) && (!line.startsWith("Use ?")))
//                    setlogOutputAux.append(line).append("\n");
//
//
//                else if(line.startsWith("_CONSTR"))
//                    sout = line +"\n"+ setlogOutputAux;
//
//
//            }
//        }

        catch (Exception e){
            e.printStackTrace();
            return null;
        }

        //System.out.println("********************************************* Terminaprolog ******** ");

        setlogOutput.append(sout); //salida setlog
        return sOutErr;
    }
//
//    public List<String> runSetLog(List<String> setlogInputList, List<StringBuilder> setlogOutputList){
//
//        try {
//            path = URLDecoder.decode(path, "UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//        List<String> sOutErrList = new ArrayList<String>();
//
//
//        StringBuilder goals= new StringBuilder();
//        String goalName;
//        for (String setlogInput : setlogInputList){
//            try {
//                path = URLDecoder.decode(path, "UTF-8");
//                // build prolog query to invoke setlog on the input
//                String goal = "consult('" + path + setLogFile + "')."
//                        + "\nset_prolog_flag(toplevel_print_options, [quoted(true), portray(true)])."
//                        + "\nuse_module(library(dialect/sicstus/timeout))."
//                        //"\nconsult('" + path + setLogLib + "')."
//                        + "\nset_prolog_flag(answer_write_options, [quoted(true), portray(true),max_depth(0)])."
//                        + "\ntime_out(setlog( \n"
//                        + setlogInput
//                        + "\n,_CONSTR)," + tout + ",_RET).\n\n";
//
//                goalName = String.valueOf(contador++) + "_goal.txt";
//                goals.append("prolog < " + path + goalName + "\n");
//                PrintWriter writer = new PrintWriter(path + goalName, "UTF-8");
//                writer.print(goal);
//                writer.close();
//            } catch (UnsupportedEncodingException e1) {
//                e1.printStackTrace();
//            } catch (FileNotFoundException e1) {
//                e1.printStackTrace();
//            }
//        }
//
//        try {
//            PrintWriter writer2 = new PrintWriter(path +"run_goals.sh", "UTF-8");
//            writer2.print(goals);
//            writer2.close();
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
//
//
//        Process proc = null;
//        try {
//            proc = Runtime.getRuntime().exec(new String[]{"sh", "/home/jmesuro/fastest/setlog/run_goals.sh"});
//
//            BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getInputStream()));
//            System.out.println("********************************************* STDERROR  ******** ");
//            //BufferedReader stdError = new BufferedReader(new InputStreamReader(proc.getErrorStream()));
//            String line;
//
//            boolean err = false;
//            while ((line = stdError.readLine()) != null) {
//                System.out.println(line);
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//
//
//        return sOutErrList;
//
//}

}
