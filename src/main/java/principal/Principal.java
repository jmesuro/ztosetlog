package principal;

/**
 * Clase sólo para las pruebas del módulo standalone
 * Para usar en fastest usar la interface principal Z2SL
 * Created by jmesuro on 01/04/16.
 */
import czt.SpecUtils;
import net.sourceforge.czt.z.ast.AxPara;
import rwrules.RWRulesLoader;
import setLog.SetLogLoader;
import translationEngine.zToProlog.Z2Prolog;
import translationEngine.zToSlog.ZToSLogTranslator;

import java.io.IOException;

public final class Principal {

   public static void main(String args[]) throws IOException {
        System.err.close();

        (new RWRulesLoader("/rwRules.tex")).loadRWRules();
        SpecLoader sl = new SpecLoader();


        try {
            (new SetLogLoader("./conf/setlog.conf")).loadconf();
            sl.loadspec(args[0]);


            //prototipos
            //Z2Prolog z2Prolog = new Z2Prolog();
            //z2Prolog.run(sl.getSpec());


            //z2setlog
            Z2SL z2SL = new Z2SL();
            ZToSLogTranslator zToSLogTranslator = new ZToSLogTranslator();
            AxPara axPara = z2SL.run(zToSLogTranslator.getAxPara(sl.getSpec()),sl.getSpec());
            System.out.println(z2SL.getSetLogCodePP());
            if (axPara != null)
                System.out.println( SpecUtils.termToLatex(axPara));


        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }

   }


/*
    public static void main(String args[]) throws IOException {
        System.err.close();

        (new RWRulesLoader("/rwRules.tex")).loadRWRules();
        SpecLoader sl = new SpecLoader();


        try {
            (new SetLogLoader("/setlog/setlog.conf")).loadconf();
            sl.loadspec(args[0]);

            ZToSLogTranslator zToSLogTranslator = new ZToSLogTranslator();

            Z2SL z2SL = new Z2SL();
            z2SL.run(zToSLogTranslator.getAxPara(sl.getSpec()),sl.getSpec());
            System.out.println(z2SL.getSetlogCode());

        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }

    }

*/

}


