package rwrules;

import net.sourceforge.czt.z.ast.DeclList;
import net.sourceforge.czt.z.ast.Expr;
import net.sourceforge.czt.z.ast.ZDeclList;

import java.util.List;

/**
 * Interface that abstracts a rewrite rule related with mathematics laws
 */
interface RWRuleLaw extends RWRule{
    /**
     * Gets the regular expression related with the law
     * @return the regular expression
     */
    String getRegEx();

    /**
     * Sets the regular expression related with the law
     * @param regex the regular expression
     */
    void setRegEx(String regex);

    /**
     * Check if an atomic predicate matches with the regular expression of the law
     * @return a boolean value with the result of the evaluation
     */
    boolean match(String predicate);

    /**
     * Sets the definition of this rewrite rule.
     * @param definition
     */
    void setDefinition(String definition);

    /**
     * Gets the definition of this rewrite rule.
     * @return
     */
    String getDefinition();

    /**
     * Sets the name of this rewrite rule
     * @param name
     */
    void setName(String name);

    /**
     * Gets the name of this rewrite rule.
     * @return
     */
    String getName();

    /**
     * Sets the list of declarations of variables
     */
	void setZDeclList(ZDeclList zDeclList);

    /**
     * Gets the list of declarations of variables
     */
	DeclList getZDeclList();

    /**
     * Gets a list with the Strings that match in the regular expression
     * @return
     */
    List<String> getStrExpr();
    /**
     * Gets the types
     * @return
     */
    List<Expr> getTypes();

    String addRegExValues(String originalExpr);
} 