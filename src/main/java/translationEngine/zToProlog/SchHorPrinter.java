package translationEngine.zToProlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;
import net.sourceforge.czt.z.visitor.*;
import translationEngine.zToProlog.prototipoState.PrototiposState;

import java.util.*;

/**
 * Created by joaquin on 27/07/17 ztosetlog.
 */
final class SchHorPrinter implements
        AndExprVisitor<String>,
        OrExprVisitor<String>,
        RefExprVisitor<String>,
        AxParaVisitor<String> {


    private String opName; //Nombre de la operación compuesta, es decir la que está del lado izquierdo
    // A = B \lor C entonces opName = "A"
    private Set<String> inputVars;

    SchHorPrinter(String opName) {
        this.opName = opName;
        this.inputVars = new HashSet<String>();

    }

    public String visitAndExpr(AndExpr andExpr) {

        StringBuilder s = new StringBuilder();
        String sl = andExpr.getLeftExpr().accept(this);
        String sr = andExpr.getRightExpr().accept(this);
        s.append(sl).append(",\n").append(sr);

        return s.toString();
    }

    public String visitOrExpr(OrExpr orExpr) {

        StringBuilder s = new StringBuilder();
        String sl = orExpr.getLeftExpr().accept(this);
        String sr = orExpr.getRightExpr().accept(this);
        s.append(sl).append("\n;\n").append(sr);

        return s.toString();
    }


    public String visitRefExpr(RefExpr refExpr) {
        String name = "refExpr";
        if (!refExpr.getMixfix() && refExpr.getZExprList().isEmpty()) //Reference
            name = refExpr.getZName().toString();

        //Si es una operación que no fue visitada, la visito
        if (!PrototiposState.alreadyVisited(name)) {
            AxPara axPara = PrototiposState.getOp(name);
            if (axPara != null) {
                // Acá no se usa la salida porque esa operación está expandida en la spec
                // y si no se imprimiría dos vececs, el tema es visitarla antes.
                PrologPrinter.printAxPara(name, axPara);
            }
        }

        Set<String> nameInputVars = PrototiposState.getInputVars(name);
        inputVars.addAll(nameInputVars);

        return "\t" + PrologUtils.buildSubOpHorSentence(name, opName, nameInputVars);
    }

    public String visitAxPara(AxPara axPara) {

        ZDeclList zDeclList = axPara.getZSchText().getZDeclList();
        String s = null;

        if (!zDeclList.isEmpty()) {
            ConstDecl constDecl = (ConstDecl) zDeclList.get(0);
            Expr expr = constDecl.getExpr();
            String s2 = expr.accept(this);

            if (expr instanceof SchExpr) {
                ZFactoryImpl zFactory = new ZFactoryImpl();
                ZNameList zNameList = zFactory.createZNameList(new ArrayList());
                AxPara axPara2 = zFactory.createAxPara(zNameList, axPara.getZSchText(), Box.SchBox);
                // Acá si usamos la salida porque es una operación horizontal, no es un nombre de operación
                // que hay que visitar antes.
                s = PrologPrinter.printAxPara(opName, axPara2);
            } else {
                String opSentence = PrologUtils.buildOpHorSentence(opName, inputVars);
                s = opSentence + ":-\n" + s2 + ".\n\n";
            }
        }


        return s;
    }


}
