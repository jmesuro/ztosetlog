package translationEngine.zToProlog;

import java.util.Set;

/**
 * Created by joaquin on 28/09/17 ztosetlog.
 */
final class PrologUtils {

    // PAsa a minúscila la inicial
    private static String schPrologName(String opName) {
        return Character.toLowerCase(opName.charAt(0)) + opName.substring(1);
    }

    static String buildOpHorSentence(String name, Set<String> l) {
        StringBuilder s = new StringBuilder();
        name = PrologUtils.schPrologName(name);
        s.append(name).append("(");
        for (String var : l)
            s.append(var).append(",");

        String ss = (s.subSequence(0, s.length() - 1)).toString();
        return ss + ")";
    }

    static String buildSubOpHorSentence(String inclName, String name, Set<String> l) {
        StringBuilder s = new StringBuilder();
        String nameL = PrologUtils.schPrologName(inclName);
        s.append(nameL).append("(").append("'").append(name).append("'");
        for (String var : l)
            s.append(",").append(var);

        s.append(")");

        return s.toString();
    }

    static String buildOpSentence(String name, Set<String> l) {
        StringBuilder s = new StringBuilder();
        name = PrologUtils.schPrologName(name);
        s.append(name).append("(").append("Tn");
        for (String var : l)
            s.append(",").append(var);

        s.append(")");
        return s.toString();
    }


}
