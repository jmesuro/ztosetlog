package translationEngine.zToProlog.prototipoState;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 26/09/17 ztosetlog.
 */
final class OpVars {
    Map<String,String> zVarSlVar;
    Map<String,String> zStateSlVar;
    Map<String,String> zStateSlPrimed; // variables de estado z --> primada en sl
    Map<String,String> zOutSlVar; // variables de estado z --> primada en sl


    OpVars(){
        zVarSlVar = new HashMap<String, String>();
        zStateSlVar = new HashMap<String, String>();
        zStateSlPrimed = new HashMap<String, String>();
        zOutSlVar = new HashMap<String, String>();

    }
}
