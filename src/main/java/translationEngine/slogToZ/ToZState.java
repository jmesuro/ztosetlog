package translationEngine.slogToZ;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joaquin on 06/10/16.
 */
public final class ToZState {

    private static int nsch = 0;

    public static void init(){
        tipos.clear();
        zNames.clear();
        zVars.clear();

    }
    public static String getSchName(){
        return "SchName" + ++nsch;
    }

    //dada una variable Z te dice su tipo z en string
    public static Map<String,String> tipos = new HashMap<String, String>();
    //mapea un nombre SLOG a un nombre Z, si es una variable auxiliar se mapea a sí misma
    public static Map<String,String> zNames = new HashMap<String, String>();
    //variables z originales, las que tienen que ser asignadas a ctes.
    public static Map<String,String> zVars = new HashMap<String, String>();
    //public static Map<String,String> schTypes = new HashMap<String,String>();
}
