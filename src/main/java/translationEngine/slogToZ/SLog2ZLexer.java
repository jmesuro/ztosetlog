// Generated from SLog2Z_2.g4 by ANTLR 4.5.3

package translationEngine.slogToZ;
import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SLog2ZLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5.3", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, T__10=11, T__11=12, T__12=13, T__13=14, T__14=15, T__15=16, T__16=17, 
		T__17=18, T__18=19, T__19=20, T__20=21, T__21=22, T__22=23, T__23=24, 
		T__24=25, NAME=26, CTE=27, NUM=28, NL=29, WS=30, SKIP_=31;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"T__0", "T__1", "T__2", "T__3", "T__4", "T__5", "T__6", "T__7", "T__8", 
		"T__9", "T__10", "T__11", "T__12", "T__13", "T__14", "T__15", "T__16", 
		"T__17", "T__18", "T__19", "T__20", "T__21", "T__22", "T__23", "T__24", 
		"NAME", "CTE", "NUM", "NL", "WS", "SKIP_"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'_CONSTR'", "'='", "'['", "','", "']'", "'set('", "')'", "'pfun('", 
		"'list('", "'integer('", "'dom('", "'dompf('", "'ran('", "'disj('", "'comp('", 
		"'un('", "'neq'", "'nin'", "'int'", "'('", "'{'", "'/'", "'}'", "'|'", 
		"'-'", null, null, null, "'\n'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, null, null, null, null, null, null, null, null, null, null, 
		null, null, "NAME", "CTE", "NUM", "NL", "WS", "SKIP_"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


		Map<String,StringBuilder> slVars = new HashMap();
		Map<String,String> zNames = new HashMap();
		Map<String,String> tipos = new HashMap();
		Map<String,String> zVars = new HashMap();
		Map<String,String> valoresProhibidos = new HashMap();
		List<String> varNoGenerar = new LinkedList<String>();
		ConstantCreator cc;

		public Map<String,StringBuilder> getSlvars(){
			return slVars;
		}

		public Map<String,String> getZVars(){
			return zVars;
		}

		public ConstantCreator getCC(){
			return cc;
		}

		public void loadTablas(Map<String,String> zVars, Map<String,String> tipos, Map<String,String> zNames){
			this.zNames = zNames;
			this.tipos = tipos;
			this.zVars = zVars;
			cc = new ConstantCreator(tipos,slVars,zNames,valoresProhibidos);
		}

		private void printHashMap(Map map){
			Iterator iterator = map.keySet().iterator();
			String key,value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null)
				   value = "nullc";
			   else
				   value = map.get(key).toString();
			   System.out.println(key + " = " + value);
			}
		}

		private void printHashMap2(Map<String,String[]> map){
			Iterator<String> iterator = map.keySet().iterator();
			String key;	String[] value;
			while (iterator.hasNext()) {
			   key = iterator.next().toString();
			   if (map.get(key) == null){
				   System.out.println(key + " = " + "nullc");
				   continue;
			   }
			   else{
				   value = map.get(key);
				   System.out.print(key + " = ");
				   for (int i = 0; i<value.length;i++)
					   System.out.print(value[i] + ",");
				   System.out.println();
			   }
			}
		}

		private void preprocesarConstraint(){
		// por que pueden venir variables Z, que solo aparezcan en constraint, no hay que llenarlas en ZVarFiller
			// por que ahi ya pueden tener valor erroneor ej constraint [V neq [], list(V)], con list V se le da valors
				if(valoresProhibidos != null){
				Iterator<String> it = valoresProhibidos.keySet().iterator();
				String var,tipo;
				StringBuilder valor;
				while (it.hasNext()) {
					var = it.next().toString();
					if (zNames != null && zNames.get(var)!=null){
						tipo = tipos.get(zNames.get(var));
						DefaultMutableTreeNode nodo = SLogUtils.toTreeNorm(tipo);
						valor = new StringBuilder(cc.getCte(var,nodo));
						if(slVars != null)
							slVars.put(var, valor);
						}
					}
				}
		}

		private void llenarZVars(){
			Iterator iterator = slVars.keySet().iterator();
			String slname,zname,valor;
			while (iterator.hasNext()) {
				slname = iterator.next().toString();
				if (slVars.get(slname)!=null){
					valor = slVars.get(slname).toString();
					zname = zNames.get(slname);
					if (zVars.containsKey(zname)){
						zVars.put(zname,valor);
					}
				}
			}
		}



	public SLog2ZLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "SLog2Z_2.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 29:
			WS_action((RuleContext)_localctx, actionIndex);
			break;
		case 30:
			SKIP__action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void WS_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:
			skip();
			break;
		}
	}
	private void SKIP__action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:
			skip();
			break;
		}
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2!\u00c6\b\1\4\2\t"+
		"\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13"+
		"\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31"+
		"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \3\2"+
		"\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3\3\3\3\3\4\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3"+
		"\7\3\7\3\7\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13"+
		"\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r\3"+
		"\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\3"+
		"\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\22\3\22\3\22\3"+
		"\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\24\3\25\3\25\3\26\3\26\3\27\3"+
		"\27\3\30\3\30\3\31\3\31\3\32\3\32\3\33\3\33\7\33\u00a9\n\33\f\33\16\33"+
		"\u00ac\13\33\3\34\3\34\7\34\u00b0\n\34\f\34\16\34\u00b3\13\34\3\35\6\35"+
		"\u00b6\n\35\r\35\16\35\u00b7\3\36\3\36\3\37\6\37\u00bd\n\37\r\37\16\37"+
		"\u00be\3\37\3\37\3 \3 \3 \3 \2\2!\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23"+
		"\13\25\f\27\r\31\16\33\17\35\20\37\21!\22#\23%\24\'\25)\26+\27-\30/\31"+
		"\61\32\63\33\65\34\67\359\36;\37= ?!\3\2\7\4\2C\\aa\6\2\62;C\\aac|\5\2"+
		"//\62;c|\5\2\62;C\\c|\5\2\13\13\17\17\"\"\u00c9\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2"+
		"\2\2\2\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3"+
		"\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3"+
		"\2\2\2\2\65\3\2\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3"+
		"\2\2\2\3A\3\2\2\2\5I\3\2\2\2\7K\3\2\2\2\tM\3\2\2\2\13O\3\2\2\2\rQ\3\2"+
		"\2\2\17V\3\2\2\2\21X\3\2\2\2\23^\3\2\2\2\25d\3\2\2\2\27m\3\2\2\2\31r\3"+
		"\2\2\2\33y\3\2\2\2\35~\3\2\2\2\37\u0084\3\2\2\2!\u008a\3\2\2\2#\u008e"+
		"\3\2\2\2%\u0092\3\2\2\2\'\u0096\3\2\2\2)\u009a\3\2\2\2+\u009c\3\2\2\2"+
		"-\u009e\3\2\2\2/\u00a0\3\2\2\2\61\u00a2\3\2\2\2\63\u00a4\3\2\2\2\65\u00a6"+
		"\3\2\2\2\67\u00ad\3\2\2\29\u00b5\3\2\2\2;\u00b9\3\2\2\2=\u00bc\3\2\2\2"+
		"?\u00c2\3\2\2\2AB\7a\2\2BC\7E\2\2CD\7Q\2\2DE\7P\2\2EF\7U\2\2FG\7V\2\2"+
		"GH\7T\2\2H\4\3\2\2\2IJ\7?\2\2J\6\3\2\2\2KL\7]\2\2L\b\3\2\2\2MN\7.\2\2"+
		"N\n\3\2\2\2OP\7_\2\2P\f\3\2\2\2QR\7u\2\2RS\7g\2\2ST\7v\2\2TU\7*\2\2U\16"+
		"\3\2\2\2VW\7+\2\2W\20\3\2\2\2XY\7r\2\2YZ\7h\2\2Z[\7w\2\2[\\\7p\2\2\\]"+
		"\7*\2\2]\22\3\2\2\2^_\7n\2\2_`\7k\2\2`a\7u\2\2ab\7v\2\2bc\7*\2\2c\24\3"+
		"\2\2\2de\7k\2\2ef\7p\2\2fg\7v\2\2gh\7g\2\2hi\7i\2\2ij\7g\2\2jk\7t\2\2"+
		"kl\7*\2\2l\26\3\2\2\2mn\7f\2\2no\7q\2\2op\7o\2\2pq\7*\2\2q\30\3\2\2\2"+
		"rs\7f\2\2st\7q\2\2tu\7o\2\2uv\7r\2\2vw\7h\2\2wx\7*\2\2x\32\3\2\2\2yz\7"+
		"t\2\2z{\7c\2\2{|\7p\2\2|}\7*\2\2}\34\3\2\2\2~\177\7f\2\2\177\u0080\7k"+
		"\2\2\u0080\u0081\7u\2\2\u0081\u0082\7l\2\2\u0082\u0083\7*\2\2\u0083\36"+
		"\3\2\2\2\u0084\u0085\7e\2\2\u0085\u0086\7q\2\2\u0086\u0087\7o\2\2\u0087"+
		"\u0088\7r\2\2\u0088\u0089\7*\2\2\u0089 \3\2\2\2\u008a\u008b\7w\2\2\u008b"+
		"\u008c\7p\2\2\u008c\u008d\7*\2\2\u008d\"\3\2\2\2\u008e\u008f\7p\2\2\u008f"+
		"\u0090\7g\2\2\u0090\u0091\7s\2\2\u0091$\3\2\2\2\u0092\u0093\7p\2\2\u0093"+
		"\u0094\7k\2\2\u0094\u0095\7p\2\2\u0095&\3\2\2\2\u0096\u0097\7k\2\2\u0097"+
		"\u0098\7p\2\2\u0098\u0099\7v\2\2\u0099(\3\2\2\2\u009a\u009b\7*\2\2\u009b"+
		"*\3\2\2\2\u009c\u009d\7}\2\2\u009d,\3\2\2\2\u009e\u009f\7\61\2\2\u009f"+
		".\3\2\2\2\u00a0\u00a1\7\177\2\2\u00a1\60\3\2\2\2\u00a2\u00a3\7~\2\2\u00a3"+
		"\62\3\2\2\2\u00a4\u00a5\7/\2\2\u00a5\64\3\2\2\2\u00a6\u00aa\t\2\2\2\u00a7"+
		"\u00a9\t\3\2\2\u00a8\u00a7\3\2\2\2\u00a9\u00ac\3\2\2\2\u00aa\u00a8\3\2"+
		"\2\2\u00aa\u00ab\3\2\2\2\u00ab\66\3\2\2\2\u00ac\u00aa\3\2\2\2\u00ad\u00b1"+
		"\t\4\2\2\u00ae\u00b0\t\5\2\2\u00af\u00ae\3\2\2\2\u00b0\u00b3\3\2\2\2\u00b1"+
		"\u00af\3\2\2\2\u00b1\u00b2\3\2\2\2\u00b28\3\2\2\2\u00b3\u00b1\3\2\2\2"+
		"\u00b4\u00b6\4\62;\2\u00b5\u00b4\3\2\2\2\u00b6\u00b7\3\2\2\2\u00b7\u00b5"+
		"\3\2\2\2\u00b7\u00b8\3\2\2\2\u00b8:\3\2\2\2\u00b9\u00ba\7\f\2\2\u00ba"+
		"<\3\2\2\2\u00bb\u00bd\t\6\2\2\u00bc\u00bb\3\2\2\2\u00bd\u00be\3\2\2\2"+
		"\u00be\u00bc\3\2\2\2\u00be\u00bf\3\2\2\2\u00bf\u00c0\3\2\2\2\u00c0\u00c1"+
		"\b\37\2\2\u00c1>\3\2\2\2\u00c2\u00c3\7^\2\2\u00c3\u00c4\7^\2\2\u00c4\u00c5"+
		"\b \3\2\u00c5@\3\2\2\2\7\2\u00aa\u00b1\u00b7\u00be\4\3\37\2\3 \3";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}