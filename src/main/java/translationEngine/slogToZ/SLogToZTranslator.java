package translationEngine.slogToZ;
import czt.SpecUtils;
import czt.UniqueZLive;
import czt.visitors.CZTCloner;
import net.sourceforge.czt.animation.eval.ZLive;
import net.sourceforge.czt.parser.z.ParseUtils;
import net.sourceforge.czt.session.CommandException;
import net.sourceforge.czt.session.StringSource;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public final class SLogToZTranslator {

	private Map<RefExpr, Expr> varExprMap;



	private  Map<String, String> fillZVars(String setLogOutput){

		ToZState.zNames.put("INT","\\num");
		ToZState.zNames.put("NAT","\\nat");
		ToZState.zNames.put("NAT1","\\nat{1}");

		ZVarsFiller zvf = new ZVarsFiller(ToZState.zVars, ToZState.tipos, ToZState.zNames, setLogOutput);

		try{
			zvf.generar();
		}

		catch (Exception e) {
			e.printStackTrace();
			System.out.println("Error when translating from {log}: " + e.toString());
			return null;
		}

		return ToZState.zVars;
	}


	public AxPara buildAxPara(String setLogOutput, AxPara axPara){
		fillZVars(setLogOutput);
		//Creamos el caso de prueba a partir de los valores de las variables obtenidas
		Map<RefExpr, Expr> map = new HashMap<RefExpr, Expr>();
		Iterator<String> keys = ToZState.zVars.keySet().iterator();
		ZLive zLive = UniqueZLive.getInstance();

		while (keys.hasNext()) {
			String varName = keys.next();
			String value = ToZState.zVars.get(varName);
			if (value != null) {
				RefExpr var;
				Expr val;
				try {


					var = (RefExpr) ParseUtils.parseExpr(new StringSource(varName), zLive.getCurrentSection(), zLive.getSectionManager());
					val = ParseUtils.parseExpr(new StringSource(value), zLive.getCurrentSection(), zLive.getSectionManager());
					map.put(var, val);

				} catch (IOException e) {
					e.printStackTrace();
				} catch (CommandException e) {
					e.printStackTrace();
				}
			}
		}
		this.varExprMap = map;

		return fillAxPara(axPara, map);
	}

	private AxPara fillAxPara(AxPara axPara, Map<RefExpr, Expr> varExprMap){
		AxPara newAxPara = (AxPara) axPara.accept(new CZTCloner());
		String schName = SpecUtils.getAxParaName(axPara) ;

		ZFactory zFactory = new ZFactoryImpl();
		ZName zName = zFactory.createZName(schName,zFactory.createZStrokeList(), "IncludedTClass");
		RefExpr refExpr = zFactory.createRefExpr(zName,zFactory.createZExprList(), false, false);
		ZDeclList zDeclList = zFactory.createZDeclList();
		zDeclList.add(0, zFactory.createInclDecl(refExpr));
		SpecUtils.setAxParaListOfDecl(newAxPara, zDeclList);
		SpecUtils.setAxParaName(newAxPara, schName);
		SpecUtils.setAxParaPred(newAxPara, SpecUtils.createAndPred(varExprMap));

		return  newAxPara;

	}


    public Map<RefExpr, Expr> getVarExprMap(){
        return this.varExprMap;
    }

}
