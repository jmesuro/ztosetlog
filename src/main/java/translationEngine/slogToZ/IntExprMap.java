package translationEngine.slogToZ;

import java.util.Map;
import javax.swing.tree.DefaultMutableTreeNode;

// Mapeador de naturales a expresion, por cada numero natural da una expresion y viceversa
final class IntExprMap {
	private Map<String, String> tipos;

	private int postfijo;
	private int CARDINF = 1000;


	private String toLowerCaseBasicType(String basicTypeName ){
		return Character.toLowerCase(basicTypeName.charAt(0)) + basicTypeName.substring(1);
	}

	private int numFromFreeType(String tipo,String elem){
		String s;
		String aux[] = tipo.split(":");
		s = aux[2].substring(1, aux[2].length()-1);
		aux = s.split(",");
		for(int i=0;i<aux.length;i++){
			if (aux[i].equals(ToZState.zNames.get(elem)))
				return i;
		}
		return 0;
	}

	private int numFromBasicType(String tipo, String elem){
		//me quedo con el número final
		//ojo acá asumo que las constantes básicas están formadas "minúscula + nombreTipo + numero"
		return Integer.valueOf(elem.split( toLowerCaseBasicType(tipo) )[1]);
	}

	private int numFromUptoType(String tipo,String elem){
		String aux[] = tipo.split("\\\\upto");
		return new Integer(elem) - new Integer(aux[0]) + 1;
	}

	private int cardOfFreeType(String tipo){
		String aux[] = tipo.split(":");
		aux = aux[2].split(",");
		return aux.length;
	}

	// devuelve la cardinalidad del tipo
	// si el numero es mayor al maximo del int devuelve 0.
	// si el tipo es infinito, devuelve CARDINF
	private int cardinalidad(DefaultMutableTreeNode nodo){
		String ct = nodo.toString();
		int c;

		if (ct.equals("\\power")){
			c = cardinalidad((DefaultMutableTreeNode) nodo.getChildAt(0));
			c = (2<<(c-1));
		}
		else if(ct.equals("\\cross")){
			c = cardinalidad((DefaultMutableTreeNode) nodo.getChildAt(0));
			c = c * cardinalidad((DefaultMutableTreeNode) nodo.getChildAt(1));
		}
		else if (ct.equals("\\num") || ct.contains("nat") || (tipos.get(ct).startsWith("BasicType"))  )
			return CARDINF;
		else
			c = cardOfFreeType(tipos.get(ct));
		return c;
	}

	//devuelve el elemento en la pos i del tipo libre de izq a derecha
	private  String elemFromFreeType(String tipo, int i){

		String s;
		String aux[] = tipo.split(":");
		s = aux[2].substring(1, aux[2].length()-1);

		aux = s.split(",");

		i = (i % aux.length) ; //esta línea hace que siempre tome un elemento, que no se pase
		return ToZState.zNames.get(aux[i]);
	}

	private String elemFromUptoType(String tipo, int i){
		String aux[] = tipo.split("\\\\upto");
		return String.valueOf(new Integer(aux[0]) +i -1 );
	}


	private class Tupla{
		int dimension;
		int[] cardinalidad;
		int[] valor;

		Tupla(int dim){
			this.dimension = dim;
			this.cardinalidad = new int[dimension];
			this.valor = new int[dimension];
		}


	}

	// dada una tupla estilo algebra lineal, devuelve un int
	private int intFromTuple_dep(Tupla t){

		// calculo la cardinalidad acumulada para cada coord
		int[] card_acs = new int[t.dimension];
		card_acs[t.dimension-1] = t.cardinalidad[t.dimension-1];

		for (int i = t.dimension - 2; i>=0; i--)
			card_acs[i] = card_acs[i+1] * t.cardinalidad[i];

		// el valor de la coord de más a la derecha
		// lo tomo como el valor menos significativo
		int salida = t.valor[t.dimension-1];

		// los siguientes valores tienen que multiplicarse por la card acumulada
		// estilo sistema decimal
		for (int i=t.dimension -2; i >= 0;i--){
			salida += (t.valor[i] - 1) * card_acs[i+1];
		}

		return salida;
	}
	// dada un int, devuelve una tupla estilo algebra lineal,
    private  Tupla tupleFromInt_dep(Tupla tupla, int num){


        int i = tupla.dimension -1; // arranca a la derecha
        int aux_val = 0;
        while (num >= tupla.cardinalidad[i] && i>0){ //entra
            aux_val = num % tupla.cardinalidad[i]; //el valor siempre es el mod por la cardinalidad de es cord
            tupla.valor[i] = aux_val == 0 ? tupla.cardinalidad[i]:aux_val; //si es cero entonces es la card
            num = (num / tupla.cardinalidad[i])  +  (aux_val > 0 ? 1:0); //actualizo el número
            i--;
        }
        tupla.valor[i] =  num ;
        for (int j=i-1;j>=0;j--)
            tupla.valor[j] = 1; //lleno de unos a la izquierda

        return tupla;
    }


	// es distinto a parFromInt porque en este la tupla tiene cada componente independiente, porque se usa
    // en schema, donde en realidad mas que tupla es un conjunto de tipos, no una matriz como en par.
	private Tupla tupleFromInt(Tupla tupla, int num){

		int dim = 0;
		int size = tupla.dimension;
		int totalCardinality = 1;

		if (num == 1) {
			tupla.valor[0] = 1;
			dim++;
		} else {
			while (num > totalCardinality) { //Si no me alcanza con las dimensiones usadas

				int value = num / totalCardinality; //Calculo cuanto me falta
				if (num % totalCardinality > 0) //Si no da justo agrego 1
					value++;
				value = value % tupla.cardinalidad[dim]; //El resto es el valor

				if (value == 0) //Si el resto es 0, entonces entra justo (uso la cardinalidad)
					tupla.valor[dim] = tupla.cardinalidad[dim];
				else
					tupla.valor[dim] = value;

				totalCardinality *= tupla.cardinalidad[dim];
				dim++;
			}

		}
		while (dim < size) { //El resto se llena en 0, es un valor invalido, pero se usa para no calcular la dimension total
			tupla.valor[dim] = 0;
			dim++;
		}

		return tupla;
	}

	private int intFromTuple(Tupla tuple){

		int dim = 1;
		int num = tuple.valor[0]; //La primera dimension suma derecho, el resto se calcula
		int size = tuple.dimension;
		int totalCardinality = tuple.cardinalidad[0];

		//Buscamos la ultima dimension que no tiene 0
		while ((dim < size) && (tuple.valor[dim]) != 0){
			int value = tuple.valor[dim];
			value--;

			value *= totalCardinality;
			num += value;

			totalCardinality *= tuple.cardinalidad[dim];
			dim++;
		}

		return num;
	}

	//devuelve la posicion de los bits encendidos de la representacion binaria del entero,
	private  int[] posEncendidas(int n){
		int[] palabra = new int[Integer.SIZE];
		int res,resto,i,ni;
		res = n>>1;
		resto = n&1;
		i = 0;ni=1;
		while(res > 0 ){
			if(resto == 1){
				palabra[i] = ni;
				i++;
			}
			resto = res&1;
			res = res>>1;ni++;
		}
		palabra[i++] = ni*resto;
		return palabra;
	}



	/* Dado un numero natural y un tipo devuelve una expresion terminal del tipo */
	private  String f(DefaultMutableTreeNode nodo, int num){
		String salida = "";
		StringBuilder s = new StringBuilder();
		String ct = nodo.toString();
		DefaultMutableTreeNode hijoIzq = null;
		if(!nodo.isLeaf())
			hijoIzq = (DefaultMutableTreeNode) nodo.getChildAt(0);

		if (ct.equals("\\num") || ct.contains("nat"))
			return String.valueOf(num);



		else if ( ct.equals("\\power") || ct.equals("\\seq") ){
			String p1,p2;
			p1 = ct.charAt(1) == 'p'?"{":"[";
			p2 = ct.charAt(1) == 'p'?"}":"]";
			String ctHijo;
			int n[]=posEncendidas(num);
			int numi,i;
			i=0;
			numi= n[i];
			int aux;
			while(numi!=0){
				ctHijo = hijoIzq.toString();
				//numi - 1 es el numero decimal que representa la constante
				//el cual es la posicion del bit encendido - 1
				aux = (ctHijo.equals("\\power")||ctHijo.equals("\\seq") )? numi-1 : numi;
				s.append(",").append(f(hijoIzq, aux));
				i++;
				numi = n[i];
			}
			if(!"".equals(s.toString()))
				salida = p1 + s.substring(1) + p2;
			else
				salida = p1 + p2;
		}

		else if (ct.equals("\\cross")){

			if (num ==0)
				salida = "[]";
			else{
				int hijos = nodo.getChildCount();
				Tupla t = new Tupla(hijos);


				for(int i = 0; i < hijos; i++) {
					DefaultMutableTreeNode node  = (DefaultMutableTreeNode) nodo.getChildAt(i);
					t.cardinalidad[i] = cardinalidad(node);
				}

				t = tupleFromInt_dep(t,num);
				int aux;
				String ctHijo;
				DefaultMutableTreeNode hijo;
				for (int i = 0 ; i < hijos; i++){
					ctHijo = nodo.getChildAt(i).toString();
					hijo = (DefaultMutableTreeNode) nodo.getChildAt(i);
					aux = (ctHijo.equals("\\power")||ctHijo.equals("\\seq") )? t.valor[i]-1 : t.valor[i];
					s.append(",").append(f(hijo, aux));
				}
				if(!"".equals(s.toString()))
					salida = "[" + s.substring(1) + "]";
				else
					salida = "[" + "]";

			}
		}

		else if (ct.contains("upto")) {
			salida = elemFromUptoType(ct,num);

		} else if(tipos.get(ct).startsWith("SchemaType:")) {
			
			String tipoCompleto = tipos.get(ct);
			ExprIterator tiposDecl = SLogUtils.schemaToTypeExprIterator(tipoCompleto);
			int tiposCant = tiposDecl.cardinalidad();
			Tupla tuple = new Tupla(tiposCant);

			for(int i = 0; i < tiposCant; i++) {
				String tipo = tiposDecl.next();
				DefaultMutableTreeNode node = SLogUtils.toTreeNorm(tipo);
				tuple.cardinalidad[i] = cardinalidad(node);
			}
			//Elegimos los valores de los elementos
			tuple = tupleFromInt(tuple,num);

			tiposDecl.reiniciar();
			for(int i = 0; i < tiposCant; i++) {
				String tipo = tiposDecl.next();
				DefaultMutableTreeNode node = SLogUtils.toTreeNorm(tipo);
				int aux;
				String nodeType = node.toString();
				//numi - 1 es el numero decimal que representa la constante
				//el cual es la posicion del bit encendido - 1
				aux = (nodeType.equals("\\power")||nodeType.equals("\\seq"))? 1 : 0;

				int nodeValue = tuple.valor[i];
				if (nodeValue == 0) //No se necesito calcular su valor, lo pongo en 1
					nodeValue = 1;

				s.append(",").append(f(node, nodeValue - aux));
			}

			if(!"".equals(s.toString()))
				salida = "[" + s.substring(1) + "]";
			else
				salida = "[" + "]";

		} else if (tipos.get(ct).startsWith("BasicType")) {
			return toLowerCaseBasicType(ct) + String.valueOf(num);
		}

		else/*tipo libre*/ {
			String tipo = tipos.get(ct);
			salida = elemFromFreeType(tipo,num);	
		}
		return salida;
	}
	/* Dado un tipo y una expresion terminal, devuelve un numero natural
	 * Ej.{a,b,c}  
	 * 	   0 0 0  | 0 1 0 1 ... 1   
	 *     0 0 1  | 0 0 1 1 ... 1
	 *     0 1 0  | 0 0 0 0 ... 1
	 *     0 1 1  | 0 0 0 0 ... 1 
	 *     1 0 0  | 0 0 0 0 ... 1
	 *     1 0 1  | 0 0 0 0 ... 1
	 *     1 1 0  | 0 0 0 0 ... 1
	 *     1 1 1  | 0 0 0 0 ....1 
	 *     los valores del lado izquiero del "|" son todos los valores posibles para una variable del tipo {a,b,c}
	 *     los del lado derecho para \power {a,b,c} y asi 
	 *     retorna -1 si no es cte*/
	private  int f(DefaultMutableTreeNode nodo, String expr){
		int salida = -1;
		String ct = nodo.toString();
		DefaultMutableTreeNode hijoIzq = null;
		if(!nodo.isLeaf())
			hijoIzq = (DefaultMutableTreeNode) nodo.getChildAt(0);

		if (ct.equals("\\num") || ct.contains("nat"))
			return Integer.parseInt(expr);



		if(ct.equals("\\power")||ct.equals("\\seq")  ){
			String ctHijo;
			ExprIterator elems = new ExprIterator(expr);
			String elem;
			int posbit;
			int ac = 0;
			int aux;
			while(elems.hasNext()){
				elem = elems.next();
				posbit = f((DefaultMutableTreeNode)nodo.getChildAt(0),elem);
				if (posbit == -1)
					return -1;
				//crea el numero a partir de las posiciones de los bit encendidos los cuales
				//pertenecen a cada elemento del conjunto
				ctHijo = hijoIzq.toString();
				aux = (ctHijo.equals("\\power")||ctHijo.equals("\\seq") )? posbit : posbit-1;
				ac |= 1<<aux; //acumula bits, cad auno corresponde a un elemento presente.
			}
			salida = ac;
		}

		else if (ct.equals("\\cross")){
			String ctHijo;
			ExprIterator elems = new ExprIterator(expr);
			String elem;
			int posbit;
			int ac = 0;
			int aux;


			int hijos = nodo.getChildCount();
			Tupla t = new Tupla(hijos);

			for(int i = 0; i < hijos; i++) {
				DefaultMutableTreeNode node  = (DefaultMutableTreeNode) nodo.getChildAt(i);
				t.cardinalidad[i] = cardinalidad(node);
			}


			int i = 0;
			DefaultMutableTreeNode hijo ;
			while(elems.hasNext()){
				elem = elems.next();
				hijo = (DefaultMutableTreeNode)nodo.getChildAt(i);
				posbit = f(hijo,elem);
				if (posbit == -1)
					return -1;

				ctHijo = hijo.toString();
				aux = (ctHijo.equals("\\power")||ctHijo.equals("\\seq") )? posbit +1: posbit;
				t.valor[i] = aux;
				i++;
			}
			salida = intFromTuple_dep(t);
		}


		else if(ct.contains("upto")){
			salida = numFromUptoType(ct,expr);
			salida = (salida == 0)?-1:salida;

		} else if(tipos.get(ct).startsWith("SchemaType:")) {

			String tipoCompleto = tipos.get(ct);
			ExprIterator tiposDecl = SLogUtils.schemaToTypeExprIterator(tipoCompleto);
			int tiposCant = tiposDecl.cardinalidad();
			Tupla tupla = new Tupla(tiposCant);
			ExprIterator elems = new ExprIterator(expr);

			for(int i = 0; i < tiposCant; i++) {
				String tipo = tiposDecl.next();
				DefaultMutableTreeNode node = SLogUtils.toTreeNorm(tipo);
				tupla.cardinalidad[i] =  cardinalidad(node);
				int fNode = f(node, elems.next());
				if (fNode == -1) //Error
					return -1;

				String ctaux = node.toString();
				fNode = (ctaux.equals("\\power")||ctaux.equals("\\seq"))? fNode+1 : fNode; //Normalizamos
				tupla.valor[i] = fNode;
			}

			return intFromTuple(tupla);

		} else if (tipos.get(ct).startsWith("BasicType")) {
			return numFromBasicType(ct, expr) ;
		}


		else/*tipo libre*/ {
			salida = numFromFreeType(tipos.get(ct),expr);
			salida = (salida == 0)?-1:salida;
		}
		return salida;
	}

	//Devuelve una expresion termino cte del tipo
	//modifica el nodo, lo normaliza ej: si es A \pfun B -> \power (A \cross B)
	String toExpr(DefaultMutableTreeNode nodo,int num){
		return f(nodo,num);
	}

	//Devuelve un numero natural que corresponde a un valor terminal del tipo 
	//modifica el nodo, lo normaliza ej: si es A \pfun B -> \power (A \cross B)
	int toNum(DefaultMutableTreeNode nodo,String expr){
		//expr = expr.replaceAll("\\s+",""); 
		return f(nodo,expr);
	}

	//es una biyeccion entre naturales y expresiones terminales ctes dado un tipo.
	IntExprMap(Map<String,String> tipos){
		this.tipos = tipos;
	}
}
