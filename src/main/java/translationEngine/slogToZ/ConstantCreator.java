package translationEngine.slogToZ;

import java.util.*;
import javax.swing.tree.DefaultMutableTreeNode;

import translationEngine.slogToZ.typeManager.TypeManagerParser;

/* La diferencia entre cte() , cteDesigual().
 * cte() toma expresiones con estructura (ej: {[A,14],Y}), que respeta para generar una cte.
 *  
 * cuando la expresion es un VARNAME (ej: X), es decir crea una cte armando la estructura, 
 * si esta en constraint y es de tipo finito se usa cteDesigual() 
 * si esta en constraint y tiene un tipo infinito se usa cteInfinita() la cual usa varias veces cteCanonica()
 * si no esta en constraint se usa solo una vez cteCanonica().
 *
 *
 * las variables setlog son con mayúsculas o con _
las constantes setlog empiezan con minúsculas
las variables z son las que están en la especificación
las constantes z se generan a partir de las constantes de setlog , son minúsculas
hay un mapa entre nombres setlog y z, las variables pueden coincidir en nombre, como el caso de las variables
liebres o basicas.


Algunas constantes las genera setlog, otras se generan acá, se generan en minúsculas igual
 * */

public final class ConstantCreator {

	private Map<String, String> tipos;
	private Map<String, String> valoresProhibidos;
	private Map<String, StringBuilder> slVars;
	private List<String> basicTypes;
	private List<String> schemaTypes;
	private Map<String, String> zNames;
	private int postfijo;
	private int MAXTRYCTEINF = 100;

	String getNumber() {
		return String.valueOf(postfijo++);
	}
	int getInt(){return postfijo++;}

	/*
	 * llena la estructura basicTypes, la cual se usa para saber si una
	 * expresion tiene como parte de su tipo un tipo basico, el cual entonces no
	 * es finito
	 */
	private void llenarBasicTypes() {
		basicTypes = new LinkedList<String>();
		Iterator<String> iterator = tipos.keySet().iterator();
		String key, valor;
		while (iterator.hasNext()) {
			key = iterator.next();
			valor = tipos.get(key);
			// EnumerationType:FT:{elem1,elem2}
			if (valor.startsWith("BasicType"))
				basicTypes.add(key);
		}
	}
	
	/*
	 * llena la estructura schemaTypes, la cual se usa para saber si una
	 * expresion tiene como parte de su tipo un tipo schema
	 */
	private void llenarSchemaTypes() {
		schemaTypes = new LinkedList<String>();
		Iterator<String> iterator = tipos.keySet().iterator();
		String key, valor;
		while (iterator.hasNext()) {
			key = iterator.next();
			valor = tipos.get(key);
			// EnumerationType:FT:{elem1,elem2}
			if (valor.startsWith("SchemaType"))
				schemaTypes.add(key);
		}
	}

	/*
	 * mantiene el invariante de valoresProhibidos. Reemplaza en todos los
	 * values la variable por la expresion instanciada.
	 */
	private void refrescarValoresProhibidos(String var, String expr) {
		Iterator<String> it = valoresProhibidos.keySet().iterator();
		String key, value;
		while (it.hasNext()) {
			key = it.next();
			value = valoresProhibidos.get(key);
			value = value.replace(var, expr);
			valoresProhibidos.put(key, value);
		}
	}


	/*
	 * instancia un valor para var, de tal manera que respete las desigualdades
	 * de valoresProhibidos esto es, no darle ningun valor que este como value
	 * en valoresProhibidos, para la key var los valores que esquiva son los que
	 * corresponden a ctes, las variables las ignora. ej: valoresProhibidos: var
	 * -> A,B,{1,2,3},{1,2}, se tiene que generar un valor para var distinto a
	 * A,B,{1,2,3} y {1,2}. como A y B no estan instanciados los ignora, mapea
	 * {1,2,3} y {1,2} a un numero y genera un numero distinto, el menor posible
	 * distinto a los mapeos de esos dos, despues lo transforma a una expresion
	 */
	private String cteDesigual(DefaultMutableTreeNode nodo, String var) {
		String salida;
		int i = 0;
		IntExprMap varMap = new IntExprMap(tipos);

		if (valoresProhibidos == null || valoresProhibidos.get(var) == null){
			return varMap.toExpr(nodo,getInt());
		}

		ExprIterator exprs = new ExprIterator("{" + valoresProhibidos.get(var)+ "}");
		int nats[] = new int[exprs.cardinalidad()];

		String expresion;

		while (exprs.hasNext()) {
			expresion = exprs.next();
			// si no es variable convierto la expresion a nat
			// porque si es variable y la quiero convertir, quizás entre en livelock por prohibiciones mutuas
			if (!SLogUtils.esSLVariableSimple(expresion)) {

				String aux_ex = getCte(expresion, nodo);
				nats[i] = varMap.toNum(nodo, aux_ex);
				i++;
			}
		}

		String tipo = nodo.toString();

		// el 0 de tipo power y seq es {},el resto de los tipos empieza en 1
		int varNat = 1; // int que sera transformado a una expresion
		if (tipo.equals("\\power") || tipo.equals("\\seq"))
			varNat = 0;

		// consigo el valor final para varNat. buscando el minimo posible
		int m = i; // m es la cantidad de constantes que fueron mapeadas a nat,
		// y a las cuales var debe ser distinta.
		i = 0; // ahora i se usa como iterador comun.
		while (i < m) {
			if (varNat == nats[i]) {
				varNat++;
				i = 0;
			} else
				i++;
		}
		salida = varMap.toExpr(nodo, varNat);
		refrescarValoresProhibidos(var, salida);

		return salida;
	}

	
	private boolean esProhibido(String var, String cte){
		ExprIterator it = new ExprIterator("{" + valoresProhibidos.get(var) + "}");
		return it.contains(cte);
	}

	/*
	 * Dada una expresion y un tipo, genera un terminal cte para el tipo
	 * respetando la estructura de la expresion ej. expr = {[a,X]} y tipo = FT
	 * \pfun \num genera {[a,1]} las variables dentro de la expresion pueden
	 * tener valores prohibidos, o valores ya calculados
	 */
	private String cte(DefaultMutableTreeNode nodo, String exprS) {

		ExprIterator expr = new ExprIterator(exprS);

		char c = exprS.charAt(0);
		String ct = nodo.toString();
		StringBuilder salida = new StringBuilder();

		if (exprS.startsWith("int(")) {

			String aux[] = exprS.substring(4, exprS.length() - 1).split(",");
			String a = cte(new DefaultMutableTreeNode("\\num"),aux[0]);
			String b = cte(new DefaultMutableTreeNode("\\num"),aux[1]);
			if (!a.equals("ValueNotAssigned") && !b.equals("ValueNotAssigned") && Integer.valueOf(b) < Integer.valueOf(a))
				return "{}";

			return "int(" + a + "," + b + ")";
		}

		if (SLogUtils.esSLVariableSimple(exprS)) {
			//String exprSOriginal = exprS; Comentado por Issue #39, no sé porque estaba este codigo originalmente
			//if (SLogUtils.esSLVariableSimple_(exprS))
			//	exprS = SLogUtils.sacar_(exprS);

			String cte;
			StringBuilder sp;
			if (slVars != null) {
				sp = slVars.get(exprS);
				if (sp != null && sp.toString() != null && !esProhibido(exprS,sp.toString()) && !sp.toString().equals(""))
					return sp.toString();
			}
			cte = cteDesigual(nodo, exprS);
			if (slVars != null)
				slVars.put(exprS, new StringBuilder(cte));
			return cte;
		}
		// si es constante la meto
		if (SLogUtils.esSLCteSimple(exprS)) {
			return exprS;
		}
		if (ct.equals("\\cross")) {
			// caso [X,Y]
			int i = 0;
			while (expr.hasNext()) {
				salida.append(",").append(cte((DefaultMutableTreeNode) nodo.getChildAt(i), expr.next()));
				i++;
			}
			while (i < nodo.getChildCount()) {
				salida.append(",").append(cte((DefaultMutableTreeNode) nodo.getChildAt(i), "X" + i));
				i++;
			}

			if (!"".equals(salida.toString()))
				return "(" + salida.substring(1) + ")";
			return "()";
		}
		if (ct.equals("\\power") || ct.equals("\\finset")) {
			// es por que el tipo \\powe(\\num\\pfun\\A) es equivalente a \\seq
			// A
			if (c == '[') {
				DefaultMutableTreeNode naux = new DefaultMutableTreeNode("\\seq");
				// powe->()->x->A (der)
				// ->NUM|NAT (izq)
				DefaultMutableTreeNode nauxHijo = (DefaultMutableTreeNode) ((nodo.getChildAt(0)).getChildAt(0)).getChildAt(1);
				naux.add(nauxHijo);
				return cte(naux, exprS);
			}

			while (expr.hasNext())
				salida.append(",").append(cte((DefaultMutableTreeNode) nodo.getChildAt(0), expr.next()));

			if (!"".equals(salida.toString()))
				return "{" + salida.substring(1) + "}";
			return "{}";

		}
		if (ct.equals("\\seq")) {
			while (expr.hasNext()){
				ExprIterator seqPar = new ExprIterator(expr.next());
				seqPar.next();
				salida.append(",").append(cte((DefaultMutableTreeNode) nodo.getChildAt(0), seqPar.next()));
			}

			if (!"".equals(salida.toString()))
				return "[" + salida.substring(1) + "]";
			return "[]";
		}
		/* es tipo esquema, porque tiene estructura y no es ningun tipo anterior */
		// pinta [X,X,X]
		ExprIterator tiposDecl = SLogUtils.schemaToTypeExprIterator(tipos.get(ct));
		ExprIterator varsDecl = SLogUtils.schemaToVarExprIterator(tipos.get(ct));
		while (expr.hasNext()) {
			varsDecl.next();
			salida.append(",").append(cte(SLogUtils.toTreeNorm(tiposDecl.next()), expr.next()));
		}
		while (tiposDecl.hasNext())
			salida.append(",").append(cte(SLogUtils.toTreeNorm(tiposDecl.next()), varsDecl.next().toUpperCase())); // Se pasa a mayus para que
		// lo tome como variable

		if (!"".equals(salida.toString()))
			return "[" + salida.substring(1) + "]";
		return "[]";

	}

	/*
	 * No resulve el siguiente estilo de casos {a,C} donde el tipo es \power FT.
	 * Es decir no genera CONJUNTOS donde los valores del mismo es solo
	 * construcciones de tipos finitos. Esto no deberia ser necesario.
	 */
	ConstantCreator(Map<String, String> tipos,
			Map<String, StringBuilder> slVars,
			Map<String, String> zNames,
			Map<String, String> valoresProhibidos) {
		this.tipos = tipos;
		this.slVars = slVars;
		this.valoresProhibidos = valoresProhibidos;
		this.basicTypes = null;
		this.postfijo = 1;
		this.zNames = zNames;
	}

	String getCte(String expr, DefaultMutableTreeNode root) {
		expr = expr.replaceAll("\\s+", ""); // quita los espacios en blanco.
 		return cte(root, expr);
	}

}
