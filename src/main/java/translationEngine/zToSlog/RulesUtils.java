package translationEngine.zToSlog;

import czt.SpecUtils;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;

import java.util.*;

/**
 * Created by jmesuro on 17/05/16.
 */

/*
*  * las variables setlog son con mayúsculas o con _
las constantes setlog empiezan con minúsculas
las variables z son las que están en la especificación
las constantes z se generan a partir de las constantes de setlog , son minúsculas
hay un mapa entre nombres setlog y z, las variables pueden coincidir en nombre, como el caso de las variables
liebres o basicas.

se generan variables auxiliares setlog, se generan constantes setlog en minúsculas
*
* */
final class RulesUtils {

    static final String NATDEF = "delay(NAT = int(0, 2147483649),false)";
    static final String NAT1DEF = "delay(NAT1 = int(1, 2147483647),false)";
    static final String INTDEF = "delay(INT = int(-2147483648, 2147483647),false)";
    private static final String NEWVAR = "VAR";
    private static final String AUXVAR = "AUX";
    private static final String CTE = "cc";

    static final String CZTINT = "ℤ";
    static final String CZTNAT = "ℕ";
    static final String CZTNAT1 = "ℕ↘1↖";

    private static final String NAT = "NAT";
    private static final String INT = "INT";
    private static final String NAT1 = "NAT1";

    private static final String RAN = "RAN";
    private static final String DOM = "DOM";
    private static final String APPLY = "APPLY";
    private static final String REV = "REV";
    private static final String TAIL = "TAIL";
    private static final String SIZE = "SIZE";
    private static final String DIFF = "DIFF";
    private static final String SET = "SET";
    private static final String TUPLE = "TUPLE";
    private static final String SQUASH = "SQUASH";
    private static final String BUN = "BUN";
    private static final String BINTERS = "BINTERS";
    private static final String COMP = "COMP";
    private static final String DRES = "DRES";
    private static final String RRES = "RRES";
    private static final String NDRES = "NDRES";
    private static final String NRRES = "NRRES";
    private static final String RIMG = "RIMG";
    private static final String OPLUS = "OPLUS";
    private static final String APPEND = "APPEND";
    private static final String EXTRACT = "EXTRACT";
    private static final String FILTER = "FILTER";
    private static final String UN = "UN";
    private static final String INTERS = "INTERS";
    private static final String INV = "INV";
    private static final String MIN = "MIN";
    private static final String MAX = "MAX";





    static String emptySet(){
        return "{}";
    }

    private static boolean isNumericExpr(Expr exprType){
        boolean s = false;
        if (exprType instanceof RefExpr){
            String type = ((RefExpr)exprType).getZName().toString();
            s = type.equals(CZTINT) || type.equals(CZTNAT) || type.equals(CZTNAT1);
        }
        return s;
    }

    static boolean isEmptySet(String sname){
        return sname.equals("∅");
    }

    static boolean isSeq(RefExpr refExpr){
        return refExpr.getZName().toString().equals("seq _ ");
    }

    static boolean isSeq1(RefExpr refExpr){
        return refExpr.getZName().toString().equals("seq _ ↘1↖");
    }

    static String basicTypeDefinition(String type){
        return "set(" + ToSlogState.getVarSlName(type) + ")";
    }

    static String freeTypeDefinition(String type){
        List<String> elems = ToSlogState.getFreeTypeElems(type);
        String elemsS;

        if (elems.isEmpty())
            elemsS = "{}";
        else {
            StringBuilder set = new StringBuilder("{");
            for (String elem : elems)
                set.append(ToSlogState.getCteSlName(elem)).append(",");
            elemsS =  set.substring(0, set.length() - 1) + "}";
        }
        return ToSlogState.getVarSlName(type) + " = " + elemsS;
    }

    /* newSetLogVar varName slVariName accesToCrossProduct
    *  tienen el efecto secundario de modificar SpecState.
    * */
    //es cuando se necesita generar una nueva variable, por colisión o por regla
    static String newSlVar(){
        String s = NEWVAR + (++ToSlogState.nvar);
        ToSlogState.addSlVar(s);
        ToSlogState.putAllVarSlVar(s,s);
        return s;
    }

    //crea una nueva vriable auxiliar con el mismo nombre Z que SL
    private static String newAuxVar(){
        String s = AUXVAR + (++ToSlogState.auxvar);
        ToSlogState.addSlVar(s);
        ToSlogState.putAllVarSlVar(s,s);
        return s;
    }


    static String makePrimed(String var) {
        return var + "′";
    }

    static boolean isStateNoPrimed(String var){
        return !(var.endsWith("?") || var.endsWith("!") || var.endsWith("′"));
    }

    //se me ocurrió repetir la primera letra
    //para evitar el problema cuadno tenemos por ejemplo
    // x,X :\num
    static String toSlVar(String zvar){
        if (zvar.contains("?"))
            zvar = zvar.replace("?","Input");
        if (zvar.contains("!"))
            zvar = zvar.replace("!","Output");
        if (zvar.contains("′"))
            zvar = zvar.replace("′","New");

        return zvar.substring(0, 1).toUpperCase() + zvar.substring(1);
    }

    static String toSlCte(String cte){
        return CTE + (++ToSlogState.ncte) + cte;
    }
    //asume que var ya es una variable setlog , es decir ya está en mayúscula.
    static String slVariName(String var, int i){
        String s = var + "_" + i;
        ToSlogState.addSlVar(s);
        ToSlogState.putAllVarSlVar(s,s);
        return s;
    }

    static String accesToCrossProduct(String crossProduct, String acces){

        String newAuxVar = crossProduct + "_" + acces;
        ToSlogState.addSlVar(newAuxVar);
        ToSlogState.putAllVarSlVar(newAuxVar,newAuxVar);

        Expr exprType = TypeUtils.prodAcces(crossProduct,Integer.parseInt(acces));
        String type =  SpecUtils.termToLatex(exprType);
        ToSlogState.putVarToAllVarZType(newAuxVar, type);
        ToSlogState.putCZTExprType(newAuxVar,exprType);
        return newAuxVar;
    }

    /*private static String groundType(String name) {
        if (ToSlogState.isInt(name))
            return INT;
        else if (ToSlogState.isNat(name))
            return NAT;
        else if (ToSlogState.isNat1(name))
            return NAT1;
        else if (ToSlogState.isBasicType(name) || ToSlogState.isFreeType(name))
            return name;
        return name;
    }

    static String groundVar(String var, String type){
        return var + " in " + groundType(ToSlogState.getSlName(type));
    }

    */

    private static String groundType(String name) {
        if (ToSlogState.isInt(name))
            return INT;
        else if (ToSlogState.isNat(name))
            return NAT;
        else if (ToSlogState.isNat1(name))
            return NAT1;
        else if (ToSlogState.isBasicType(name) || ToSlogState.isFreeType(name))
            return name;
        return name;
    }

    static String groundVar(String var, String type){
        String slType = ToSlogState.getSlName(type);
        if (ToSlogState.isInt(ToSlogState.getSlName(slType)))
            return "delay(labeling(" + var + "),false)";
        else if (ToSlogState.isNat(slType))
            return "delay(labeling(" + var + "),false)";
        else if (ToSlogState.isNat1(slType))
            return "delay(labeling(" + var + "),false)";
        else if (ToSlogState.isBasicType(slType) || ToSlogState.isFreeType(slType))
            return var + " in " + slType;
        return var + " in " + slType;
    }

    static String equality(String left, String right) {
        return left + " = " + right;
    }

    static String MembershipPredicate(String left, String right) {
        return left + " in " + right;
    }

    static String tuple(List<String> elems) {
        String salida;
        if (elems.isEmpty())
            salida = "[]";
        else {
            StringBuilder set = new StringBuilder("[");
            for (String elem : elems)
                set.append(elem).append(",");
            salida =  set.substring(0, set.length() - 1) + "]";
        }
        return salida;
    }

    // es identico a tuple, pero genera una variable auxiliar y guarda el tipo
    static String tuple(List<String> elems, List<String> lines) {

        String var;
        String nVar = newAuxVar();


        if (elems.isEmpty()){
            nVar = "[]";
            lines.add("");
        }
        else {
            StringBuilder set = new StringBuilder("[");
            for (String elem : elems)
                set.append(elem).append(",");
            var =  set.substring(0, set.length() - 1) + "]";

            if (ToSlogState.containsAuxVarSlOpAuxVar(TUPLE,var))
                nVar = ToSlogState.getAuxVarSlOpAuxVar(TUPLE,var);
            else{
                ToSlogState.putSlOpAuxVar(TUPLE, var, nVar);

                List<Expr> elemsTypes = new ArrayList<Expr>();
                for (String elem:elems)
                    elemsTypes.add(ToSlogState.getSlExprType(elem));
                Expr tupleType = TypeUtils.prod(elemsTypes); // el tipo de la tupla, necesito todos los tipos

                ToSlogState.putVarToAllVarZType(nVar, SpecUtils.termToLatex(tupleType));
                ToSlogState.putCZTExprType(nVar,tupleType);
                lines.add(nVar + " = " + var);
            }
        }
        return nVar;
    }


    static String setExtension(List<String> elems, List<String> lines) {

        String var;
        String nVar = newAuxVar();


        if (elems.isEmpty()){
            nVar = "{}";
                lines.add("");
        }
        else {
            StringBuilder set = new StringBuilder("{");
            for (String elem : elems)
                set.append(elem).append(",");
            var =  set.substring(0, set.length() - 1) + "}";

            if (ToSlogState.containsAuxVarSlOpAuxVar(SET,var))
                nVar = ToSlogState.getAuxVarSlOpAuxVar(SET,var);
            else{
                ToSlogState.putSlOpAuxVar(SET, var, nVar);
                Expr setType = TypeUtils.set(elems.get(0)); // el tipo del conjunto es el tipo de de cualquier elemento
                ToSlogState.putVarToAllVarZType(nVar, SpecUtils.termToLatex(setType));
                ToSlogState.putCZTExprType(nVar,setType);
                lines.add(nVar + " = " + var);
            }
        }
        return nVar;
    }

    static String crossProduct(String var, List<String> ivars, List<String> lines){
        return var + " = " + tuple(ivars, lines);
    }

    static String schemaType(String var, List<String> ivars, List<String> lines){
          return var + " = " + RulesUtils.tuple(ivars, lines);
    }

    static String accesToSchemaType(String vari){
        return ToSlogState.getVarSlName(vari);
    }

   private static void auxFunRelBinRel(List<String> lines, String var, String right){
       Expr ranType = TypeUtils.rightSon(var);
       ZFactoryImpl zFactory = new ZFactoryImpl();
       ranType = zFactory.createPowerExpr(ranType);
       String nvar;
       if (ToSlogState.containsAuxVarSlOpAuxVar(RAN,var))
           nvar = ToSlogState.getAuxVarSlOpAuxVar(RAN,var);
       else{
           nvar = newAuxVar();
           lines.add("ran(" + var + ","+ nvar +")");
           ToSlogState.putSlOpAuxVar(RAN,var,nvar);
           ToSlogState.putVarToAllVarZType(nvar, SpecUtils.termToLatex(ranType));
           ToSlogState.putCZTExprType(nvar,ranType);
       }
       lines.add("subset(" + nvar +","+ groundType(right) +")");
   }
   private static String auxSeqBinRel(List<String> lines, String var){
       String nvarDom;
       if (ToSlogState.containsAuxVarSlOpAuxVar(DOM,var))
           nvarDom = ToSlogState.getAuxVarSlOpAuxVar(DOM,var);
       else{
           nvarDom = newAuxVar();
           lines.add("dom(" + var + "," + nvarDom + ")");
           ToSlogState.putSlOpAuxVar(DOM,var,nvarDom);
           ToSlogState.putVarToAllVarZType(nvarDom, SpecUtils.termToLatex(TypeUtils.powerNum()));
           ToSlogState.putCZTExprType(nvarDom,TypeUtils.powerNum());
       }
       return nvarDom;
   }


   static void binRel(String var, String left, String right, String name, List<String> lines) {
       String type = "TYPE";

       if (     name.equals(" _ → _ ") || name.equals(" _ ↣ _ ")  || name.equals(" _ ⤖ _ ") ||
                name.equals(" _ ↠ _ ") || name.equals(" _ ⤔ _ ") || name.equals(" _ ⤕ _ ") ||
                name.equals(" _ ⇻ _ ")){

           lines.add("pfun(" + var + ")");
           Expr ranType = TypeUtils.rightSon(var);
           if (isNumericExpr(ranType))
               auxFunRelBinRel(lines, var,right); //Cambio de Maxi que no está en el documento z2setlog.pdf
           else{
               String nvar;
               if (ToSlogState.containsAuxVarSlOpAuxVar(DOM,var))
                   nvar = ToSlogState.getAuxVarSlOpAuxVar(DOM,var);
               else{
                   nvar = newAuxVar();
                   lines.add("dom(" + var + "," + nvar + ")");
                   ToSlogState.putSlOpAuxVar(DOM,var,nvar);
                   Expr domType = TypeUtils.leftSon(var);
                   ToSlogState.putVarToAllVarZType(nvar, SpecUtils.termToLatex(domType));
                   ToSlogState.putCZTExprType(nvar,domType);
               }
               lines.add(groundType(left) + " = " + nvar);
           }
       }

       else if (name.equals(" _ ↔ _ ")){
           lines.add("rel(" + var + ")");
           Expr ranType = TypeUtils.rightSon(var);
           if (isNumericExpr(ranType))
               auxFunRelBinRel(lines, var,right);
       }

       else if (name.equals(" _ ⇸ _ "))
           type = "pfun(" + var + ")";

       else if (name.equals("seq _ ")){
           type = "seq";
           lines.add("pfun(" + var + ")");
           String nvarDom = auxSeqBinRel(lines,var);
           String nvarInt = newAuxVar();
           lines.add(nvarDom + " = int(1,"+nvarInt+")");
           lines.add( groundVar(nvarInt, NAT) );
           ToSlogState.putVarToAllVarZType(nvarInt, SpecUtils.termToLatex(TypeUtils.nat()));
           ToSlogState.putCZTExprType(nvarInt,TypeUtils.nat());
       }

       else if (name.equals("seq _ ↘1↖")){
           type = "seq";
           lines.add("pfun(" + var + ")");
           String nvarDom = auxSeqBinRel(lines,var);
           String nvarInt = newAuxVar();
           lines.add(nvarDom + " = int(1,"+nvarInt+")");
           lines.add( groundVar(nvarInt, NAT1) );
           ToSlogState.putVarToAllVarZType(nvarInt, SpecUtils.termToLatex(TypeUtils.nat1()));
           ToSlogState.putCZTExprType(nvarInt,TypeUtils.nat1());
       }
       if (lines.isEmpty())
           lines.add(type);

   }

    private static String auxApplyApplication(String fun, String op,List<String> lines){
        String applyOp = APPLY + "_" + fun;
        String newAuxVar;
        if (ToSlogState.containsAuxVarSlOpAuxVar(applyOp,op))
            newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(applyOp,op);
        else{
            newAuxVar = newAuxVar();
            ToSlogState.putSlOpAuxVar(applyOp,op,newAuxVar);
            lines.add("apply(" + fun + "," + op + "," + newAuxVar + ")");
        }
        return newAuxVar;
    }

    private static String auxSizeApplication(String op, List<String> lines){
        String var2;
        if(ToSlogState.containsAuxVarSlOpAuxVar(SIZE,op))
            var2 = ToSlogState.getAuxVarSlOpAuxVar(SIZE,op);
        else{
            var2 = newAuxVar();
            ToSlogState.putSlOpAuxVar(SIZE,op,var2);
            ToSlogState.putVarToAllVarZType(var2, SpecUtils.termToLatex(TypeUtils.nat()));
            ToSlogState.putCZTExprType(var2,TypeUtils.nat());
            lines.add("size(" + op + "," + var2 + ")");
            lines.add(groundVar(var2, NAT));
        }
        return var2;
    }

    static String application(String fun, String op, List<String> lines){
        String newAuxVar;
        Expr exprType;
        String type;

        if (ToSlogState.isSequence(fun))
        {
            newAuxVar = auxApplyApplication(fun,op,lines);
            exprType = TypeUtils.elem(fun);
        }

        else if (fun.equals("dom"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(DOM,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(DOM,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(DOM,op,newAuxVar);
                lines.add("dom(" + op + "," + newAuxVar + ")");
            }
            exprType = TypeUtils.dom(op);
        }

        else if (fun.equals("ran"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(RAN,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(RAN,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(RAN,op,newAuxVar);
                lines.add("ran(" + op + "," + newAuxVar + ")");
            }
            exprType = TypeUtils.ran(op);
        }

        else if (fun.equals("rev"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(REV,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(REV,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(REV,op,newAuxVar);
                lines.add("reverse(" + op + "," + newAuxVar + ")");
            }
            exprType = TypeUtils.inv(op);
        }

        else if (fun.equals("head"))
        {
            String applyOp = APPLY + "_1";
            if(ToSlogState.containsAuxVarSlOpAuxVar(applyOp,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(applyOp,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(applyOp,op,newAuxVar);
                lines.add("apply(" + op + ",1," + newAuxVar + ")");
            }
            exprType = TypeUtils.elem(op);
        }

        else if (fun.equals("last"))
        {
            String var2 = auxSizeApplication(op,lines);
            newAuxVar = auxApplyApplication(var2,op,lines);
            exprType = TypeUtils.elem(op);
        }

        else if (fun.equals("tail"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(TAIL,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(TAIL,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(TAIL,op,newAuxVar);
                lines.add("tail(" + op + "," + newAuxVar() + ")");
            }
            exprType = ToSlogState.getSlExprType(op);
        }

        else if (fun.equals("front"))
        {
            String var2 = auxSizeApplication(op,lines);

            if (ToSlogState.containsAuxVarSlOpAuxVar(DIFF,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(DIFF,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(DIFF,op,newAuxVar);
                lines.add("diff(" + op + "," + "{[" + var2 + "," + "_]}," + newAuxVar + ")");
            }
            exprType = ToSlogState.getSlExprType(op);
        }

        else if (fun.equals("squash"))
        {
            if (ToSlogState.containsAuxVarSlOpAuxVar(SQUASH,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(SQUASH,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(SQUASH,op,newAuxVar);
                lines.add("squash(" + op + "," + newAuxVar+ ")");
            }
            exprType = ToSlogState.getSlExprType(op);
        }

        else if (fun.equals("⋃"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(BUN,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(BUN,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(BUN,op,newAuxVar);
                lines.add("bun(" + op + "," + newAuxVar + ")");
            }

            exprType = ToSlogState.getSlExprType(op);
        }

        else if (fun.equals("⋂"))
        {
            if (ToSlogState.containsAuxVarSlOpAuxVar(BINTERS,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(BINTERS,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(BINTERS,op,newAuxVar);
                lines.add("binters(" + op + "," + newAuxVar + ")");
            }
            exprType = ToSlogState.getSlExprType(op);
        }

        else
        { // funcionApplication
            newAuxVar = auxApplyApplication(fun,op,lines);
            exprType = TypeUtils.rightSon(fun);
        }

        type =  SpecUtils.termToLatex(exprType);
        if (isNumericExpr(exprType)){
            String cztt = ((RefExpr)exprType).getZName().toString();
            lines.add(groundVar(newAuxVar,cztt));
        }

        ToSlogState.putVarToAllVarZType(newAuxVar, type);
        ToSlogState.putCZTExprType(newAuxVar,exprType);
        return newAuxVar;
    }

    static String functionOperatorApplication(String op, String opl, String opr, List<String> lines){
        String newAuxVar;
        Expr exprType;
        String type;
        StringBuilder tr = new StringBuilder();
        if (op.equals(" _ ⨾ _ "))
        {
            String compOp = COMP + "_" + opl;
            if (ToSlogState.containsAuxVarSlOpAuxVar(compOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(compOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(compOp,opr,newAuxVar);
                tr.append("comp(").append(opr).append(",").append(opl).append(",").append(newAuxVar).append(")");
            }
            exprType = TypeUtils.comp(opl,opr);
        }

        else if (op.equals(" _ ∘ _ "))
        {
            String compOp = COMP + "_" + opl;
            if (ToSlogState.containsAuxVarSlOpAuxVar(compOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(compOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(compOp,opr,newAuxVar);
                tr.append("comp(").append(opr).append(",").append(opl).append(",").append(newAuxVar).append(")");
            }
            exprType = TypeUtils.comp(opl,opr);
        }

        else if (op.equals(" _ ◁ _ "))
        {
            String dresOp = DRES + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(dresOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(dresOp,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(dresOp,op,newAuxVar);
                tr.append("dres(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opr);
        }

        else if(op.equals(" _ ▷ _ "))
        {
            String rresOp = RRES + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(rresOp,opl))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(rresOp,opl);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(rresOp,opl,newAuxVar);
                tr.append("rres(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        else if (op.equals(" _ ⩤ _ "))
        {
            String ndresOp = NDRES + "_" + opl;
            if (ToSlogState.containsAuxVarSlOpAuxVar(ndresOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(ndresOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(ndresOp,opr,newAuxVar);
                tr.append("ndres(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }

            exprType = ToSlogState.getSlExprType(opr);
        }
        else if (op.equals(" _ ⩥ _ "))
        {
            String nrresOp = NRRES + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(nrresOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(nrresOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(nrresOp,opr,newAuxVar);
                tr.append("nrres(").append(opr).append(",").append(opl).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        else if (op.equals(" _ ⦇ _ ⦈"))
        {
            String rimgOp = RIMG + "_" + opl;
            if (ToSlogState.containsAuxVarSlOpAuxVar(rimgOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(rimgOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(rimgOp,opr,newAuxVar);
                tr.append("rimg(").append(opr).append(",").append(opl).append(",").append(newAuxVar).append(")");
            }
            exprType = TypeUtils.rightSon(opr);
        }
        else if (op.equals(" _ ⊕ _ "))
        {
            String oplusOp = OPLUS + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(oplusOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(oplusOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(oplusOp,opr,newAuxVar);
                tr.append("oplus(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opr);
        }
        else if (op.equals(" _ ⁀ _ "))
        {
            String appendOp = APPEND + "_" + opl;
            if (ToSlogState.containsAuxVarSlOpAuxVar(appendOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(appendOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(appendOp,opr,newAuxVar);
                tr.append("append(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opr);
        }
        else if (op.equals(" _ ↿ _ "))
        {
            String extractOp = EXTRACT + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(extractOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(extractOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(extractOp,opr,newAuxVar);
                tr.append("extract(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        else if (op.equals(" _ ↾ _ "))
        {
            String filterOp = FILTER + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(filterOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(filterOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(filterOp,opr,newAuxVar);
                tr.append("filter(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opr);
        }
        else if (op.equals(" _ .. _ "))
        {
            newAuxVar = newAuxVar();
            tr.append(newAuxVar).append(" = int(").append(opl).append(",").append(opr).append(")");
            exprType = TypeUtils.powerNum();
        }
        else if (op.equals(" _ ∪ _ "))
        {
            String unOp = UN + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(unOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(unOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(unOp,opr,newAuxVar);
                tr.append("un(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        else if(op.equals(" _ ∩ _ "))
        {
            String intersOp = INTERS + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(intersOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(intersOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(intersOp,opr,newAuxVar);
                tr.append("inters(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        else if (op.equals(" _ ∖ _ "))
        {
            String diffOp = DIFF + "_" + opl;
            if(ToSlogState.containsAuxVarSlOpAuxVar(diffOp,opr))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(diffOp,opr);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(diffOp,opr,newAuxVar);
                tr.append("diff(").append(opl).append(",").append(opr).append(",").append(newAuxVar).append(")");
            }
            exprType = ToSlogState.getSlExprType(opl);
        }
        // \mapsto
        else if(op.equals(" _ ↦ _ ")){
            //newAuxVar = newAuxVar();
            List<Expr> l1 = new ArrayList<Expr>();
            List<String> l2 = new ArrayList<String>();

            l1.add( ToSlogState.getSlExprType(opl));
            l1.add( ToSlogState.getSlExprType(opr));

            l2.add(opl);
            l2.add(opr);

            newAuxVar = tuple(l2, lines);

            //tr.append(newAuxVar).append(" = ").append(tuple(l2, lines));

            ZFactory zFactory = new ZFactoryImpl();
            ExprList exprList = zFactory.createZExprList(l1);
            exprType = zFactory.createProdExpr(exprList);
        }
        else {
            // Esto no se repite por la condicion puesta al principio puesta en ExprPredPrinter applExpr
            newAuxVar = newAuxVar();
            tr.append(newAuxVar).append(" is ").append(arit(op, opl, opr));
            exprType = TypeUtils.num();
        }
        type =  SpecUtils.termToLatex(exprType);
        if (isNumericExpr(exprType)){
            String cztt = ((RefExpr)exprType).getZName().toString();
            String aux = groundVar(newAuxVar,cztt);
            if (!lines.contains(aux)) lines.add(aux);
        }
        ToSlogState.putVarToAllVarZType(newAuxVar, type);
        ToSlogState.putCZTExprType(newAuxVar,exprType);
        lines.add(tr.toString());
        return newAuxVar;
    }

    private static String arit(String op, String opl, String opr){
        String tr = "";
        if (op.equals(" _ + _ ")){
            tr = opl + " + " + opr;
        }else if (op.equals(" _ − _ ")){
            tr = opl + " - " + opr;
        }else if (op.equals(" _ * _ ")){
            tr = opl + " * " + opr;
        }else if (op.equals(" _ div _ ")){
            tr = opl + " div " + opr;
        }else if (op.equals(" _ mod _ ")){
            tr = opl + " mod " + opr;
        }
        return tr;
    }


    static String functionOperatorApplication(String fun, String op, List<String> lines){
        String newAuxVar = newAuxVar();
        Expr exprType=null;
        String type;
        if (fun.equals(" _ ∼"))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(INV,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(INV,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(INV,op,newAuxVar);
                lines.add("inv(" + op + "," + newAuxVar + ")");
            }
            exprType = TypeUtils.inv(op);
        }
        else if (fun.equals("# _ "))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(SIZE,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(SIZE,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(SIZE,op,newAuxVar);
                lines.add("size(" + op + "," + newAuxVar + ")");
                lines.add(groundVar(newAuxVar,NAT));
            }
            exprType = TypeUtils.nat();
        }
        else if (fun.equals("min _ "))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(MIN,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(MIN,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(MIN,op,newAuxVar);
                lines.add("min(" + op + "," + newAuxVar + ")");
                lines.add(groundVar(newAuxVar,INT));
            }
            exprType = TypeUtils.num();
        }
        else if (fun.equals("max _ "))
        {
            if(ToSlogState.containsAuxVarSlOpAuxVar(MAX,op))
                newAuxVar = ToSlogState.getAuxVarSlOpAuxVar(MAX,op);
            else{
                newAuxVar = newAuxVar();
                ToSlogState.putSlOpAuxVar(MAX,op,newAuxVar);
                lines.add("max(" + op + "," + newAuxVar + ")");
                lines.add(groundVar(newAuxVar,INT));
            }
            exprType = TypeUtils.num();
        }

        type =  SpecUtils.termToLatex(exprType);
        if (isNumericExpr(exprType)){
            String cztt = ((RefExpr)exprType).getZName().toString();
            String aux = groundVar(newAuxVar,cztt);
            if (!lines.contains(aux)) lines.add(aux);
        }
        ToSlogState.putVarToAllVarZType(newAuxVar, type);
        ToSlogState.putCZTExprType(newAuxVar,exprType);
        return newAuxVar;
    }


    //No me preocupo por las repeticiones de estas operaciones e la traducción, porque no generan variablea auxiliares
    //quiere decir que si vuelve a aparecer es poruqe tiene que volver a aparecer, no sintetiza ningún resultado
    static String otherOperatorApplication(String op, String opl, String opr){
        String tr = "otherOperatorApplication";
        if (op.equals(" _ ≠ _ ")){
            tr = opl + " neq " + opr;
        }else if (op.equals(" _ ∉ _ ")){
            tr = opl + " nin " + opr;
        }else if(op.equals(" _ ⊂ _ ")){
            tr = "ssubset(" + opl + "," + opr + ")";
        }else if(op.equals(" _ ⊆ _ ")){
            tr = "subset(" + opl + "," + opr + ")";
        }else if(op.equals(" _ prefix _ ")){
            tr = "prefix(" + opl + "," + opr + ")";
        }else if(op.equals(" _ suffix _ ")){
            tr = "suffix(" + opl + "," + opr + ")";
        }else if(op.equals(" _ > _ ")){
            tr = opl + " > " + opr;
        }else if(op.equals(" _ < _ ")){
            tr = opl + " < " + opr;
        }else if(op.equals(" _ ≤ _ ")){
            tr = opl + " =< " + opr;
        }else if(op.equals(" _ ≥ _ ")){
            tr = opl + " >= " + opr;
        }
        return tr;
    }

    //No me preocupo por las repeticiones de estas operaciones e la traducción, porque no generan variablea auxiliares
    //quiere decir que si vuelve a aparecer es poruqe tiene que volver a aparecer, no sintetiza ningún resultado
    static String otherOperatorApplicationNegated(String op, String opl, String opr){
        String tr = "otherOperatorApplication";

        if(op.equals(" _ ⊂ _ ")){
            String var = newAuxVar();

            ToSlogState.putVarToAllVarZType(var, ToSlogState.getZTypeAuxVarZType(op));
            ToSlogState.putCZTExprType(var, ToSlogState.getSlExprType(op));

            tr = "inters(" + opl + "," + opr + "," + var + ") & " + var + " neq " + opl ;
        }else if(op.equals(" _ ⊆ _ ")){
            tr = "nsubset(" + opl + "," + opr + ")";
        }else if(op.equals(" _ ≤ _ ")){
            tr = otherOperatorApplication(" _ > _ ",opl,opr);
        }else if(op.equals(" _ > _ ")){
            tr = otherOperatorApplication(" _ ≤ _ ",opl,opr);
        }else if(op.equals(" _ ≥ _ ")){
            tr = otherOperatorApplication(" _ < _ ",opl,opr);
        }else if(op.equals(" _ < _ ")){
            tr = otherOperatorApplication(" _ ≥ _ ",opl,opr);
        }



        return tr;
    }

    static String setComp(String decl, List<String> varList, String filter, String pattern, List<String> lines){

        return "ris(" + decl + "," + tuple(varList) + "," + filter + "," + pattern + ")";
    }

    static String orPred(String l, String r){
        return "(" + l + " or " + r + ")";
    }

    static String powerSet(String var, String type, List<String> lines ){
        if (ToSlogState.isFreeType(type))
            lines.add("subset(" + var + "," + type +")");
        return "set(" + var + ")";
    }
}