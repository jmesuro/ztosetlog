package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;

import java.util.List;

/**
 * Created by joaquin on 27/10/16.
 */
final class TypeUtils {

    // Dado dos variables de tipo RefExpr por ej:
    // x : A \rel B  y : B \rel C
    // devuelve A \rel C
    static Expr comp(String opl, String opr ){
        Expr exprl = ToSlogState.getSlExprType(opl);
        Expr exprr = ToSlogState.getSlExprType(opr);
        Expr exprll = null;
        Expr exprrr = null;

        // fun pfun pinl inj psurj surj bij rel

        if (exprl instanceof RefExpr){
            RefExpr refExpr = (RefExpr) exprl;
            if ( refExpr.getMixfix() && refExpr.getExplicit()){
                exprll = (Expr)refExpr.getExprList().getChildren()[0];
            }
        }
        else if (exprl instanceof PowerExpr)
            exprll = ((ProdExpr)(((PowerExpr)exprl).getExpr())).getZExprList().get(0);

        if (exprr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) exprr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()){
                exprrr = (Expr)refExpr.getExprList().getChildren()[1];
            }
        }
        else if (exprr instanceof PowerExpr)
            exprrr = ((ProdExpr)(((PowerExpr)exprr).getExpr())).getZExprList().get(1);

        ZFactory zFactory = new ZFactoryImpl();
        ZExprList zExprList = zFactory.createZExprList();
        zExprList.add(exprll);
        zExprList.add(exprrr);

        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName(" _ ↔ _ ",strokeList,"1593");

        return zFactory.createRefExpr(zName,zExprList,true,true);
    }

    static Expr rightSon(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr exprr=null;
        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) expr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()) {
                exprr = (Expr) refExpr.getExprList().getChildren()[1];
            }

        }
        else if (expr instanceof PowerExpr)
            exprr = ((ProdExpr)(((PowerExpr)expr).getExpr())).getZExprList().get(1);
        return exprr;
    }
    static Expr leftSon(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr exprl=null;
        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) expr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()) {
                exprl = (Expr) refExpr.getExprList().getChildren()[0];
            }

        }
        else if (expr instanceof PowerExpr)
            exprl = ((ProdExpr)(((PowerExpr)expr).getExpr())).getZExprList().get(0);
        return exprl;
    }

    static Expr prodAcces(String var, int acces){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr expri=null;
        if (expr instanceof ProdExpr)
            expri = ((ProdExpr)expr).getZExprList().get(acces-1);
        return expri;
    }

    static Expr set(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        return (new ZFactoryImpl()).createPowerExpr(expr);
    }

    static Expr prod(List<Expr> varsTypes){
        ZFactory zFactory = new ZFactoryImpl();
        ZExprList zExprList = zFactory.createZExprList();
        zExprList.addAll(varsTypes);
        return (new ZFactoryImpl()).createProdExpr(zExprList);
    }

    static Expr ran(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr exprr=null;
        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) expr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()) {
                if (((RefExpr) expr).getZName().getWord().equals("seq _ "))
                    exprr = (Expr) refExpr.getExprList().getChildren()[0];
                else
                    exprr = (Expr) refExpr.getExprList().getChildren()[1];
            }

        }
        else if (expr instanceof PowerExpr)
            exprr = ((ProdExpr)(((PowerExpr)expr).getExpr())).getZExprList().get(1);
        return (new ZFactoryImpl()).createPowerExpr(exprr);
    }

    static Expr dom(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr exprl=null;
        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) expr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()) {
                if (((RefExpr) expr).getZName().getWord().equals("seq _ "))
                    return powerNum();
                else
                    exprl = (Expr) refExpr.getExprList().getChildren()[0];
            }

        }
        else if (expr instanceof PowerExpr)
            exprl = ((ProdExpr)(((PowerExpr)expr).getExpr())).getZExprList().get(0);

        return (new ZFactoryImpl()).createPowerExpr(exprl);
    }

    static Expr powerNum(){
        ZFactory zFactory = new ZFactoryImpl();
        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName("ℤ",strokeList,"2310");
        ZExprList zExprList = zFactory.createZExprList();
        RefExpr refExpr = zFactory.createRefExpr(zName,zExprList,false,false);
        return zFactory.createPowerExpr(refExpr);
    }

    static Expr num(){
        ZFactory zFactory = new ZFactoryImpl();
        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName("ℤ",strokeList,"2310");
        ZExprList zExprList = zFactory.createZExprList();
        return zFactory.createRefExpr(zName,zExprList,false,false);
    }



    static Expr nat(){
        ZFactory zFactory = new ZFactoryImpl();
        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName("ℕ",strokeList,"1533");
        ZExprList zExprList = zFactory.createZExprList();
        return zFactory.createRefExpr(zName,zExprList,false,false);
    }

    static Expr nat1(){
        ZFactory zFactory = new ZFactoryImpl();
        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName("ℕ",strokeList,"2412");
        ZExprList zExprList = zFactory.createZExprList();
        return zFactory.createRefExpr(zName,zExprList,false,false);
    }

    static Expr inv(String var){
        Expr expr= ToSlogState.getSlExprType(var);
        Expr exprl = null;
        Expr exprr = null;

        // fun pfun pinl inj psurj surj bij rel

        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr) expr;
            if ( refExpr.getMixfix() && refExpr.getExplicit()){
                exprl = (Expr)refExpr.getExprList().getChildren()[0];
                exprr = (Expr)refExpr.getExprList().getChildren()[1];

            }
        }

        ZFactory zFactory = new ZFactoryImpl();
        ZExprList zExprList = zFactory.createZExprList();
        zExprList.add(exprr);
        zExprList.add(exprl);

        StrokeList strokeList = zFactory.createZStrokeList();
        ZName zName = zFactory.createZName(" _ ↔ _ ",strokeList,"1593");

        return zFactory.createRefExpr(zName,zExprList,true,true);


    }

    //por ahora funciona con seuqencias, devuelve el tipo del elemento
    static Expr elem(String var){
        Expr expr = ToSlogState.getSlExprType(var);
        Expr exprElem = null;

        if (expr instanceof RefExpr){
            RefExpr refExpr = (RefExpr)expr;
            if(refExpr.getMixfix() && refExpr.getExplicit()){
                exprElem =  refExpr.getZExprList().get(0);
            }
        }
        return exprElem;
    }


}
