package translationEngine.zToSlog;

import czt.SchemeType;
import czt.SpecUtils;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.util.Factory;
import net.sourceforge.czt.z.visitor.*;
import translationEngine.zToSlog.writer.Writer;
import translationEngine.zToSlog.writer.ZSchTextWriter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by joaquin on 05/07/16.
 * Tiene en cuenta el Root del Expr, es porque esa información
 * es necesaria para la traduccion de varDecls, por ej en crossProduct
 * donde se necesita la estructura general de la traducción del ProdExpr
 * x : ProdExpr
 */
final class ExprDeclPrinter implements
        ProdExprVisitor<String>,
        RefExprVisitor<String>,
        SchExprVisitor<String>,
        SetExprVisitor<String>,
        TupleSelExprVisitor<String>,
        PowerExprVisitor<String>,
        ApplExprVisitor<String>,
        BindSelExprVisitor<String>{

    private String slvar; //visitProdExpr visitRefExpr visitSchExpr
    private Factory factory;
    private ExprPredPrinter exprPredPrinter;
    private Writer writer;

    void setSlVar(String slvar) {
        this.slvar = slvar;
    }

    ExprDeclPrinter(Writer writer) {
        factory = new Factory();
        exprPredPrinter = new ExprPredPrinter(writer);
        this.writer = writer;
    }

    public String visitProdExpr(ProdExpr prodExpr) {
        VarDecl varDecli;
        List<Name> inameList;
        List<String> ivars;
        String ivar;
        ivars = new ArrayList<String>();
        int i = 1;
        for (Expr expri : prodExpr.getZExprList()) {
            inameList = new ArrayList<Name>();
            ivar = RulesUtils.slVariName(slvar, i); // x1, x2

            ToSlogState.putVarToAllVarZType(ivar, SpecUtils.termToLatex(expri));
            ToSlogState.putCZTExprType(ivar,expri);

            ivars.add(ivar);
            inameList.add(factory.createZName(ivar));
            varDecli = factory.createVarDecl(factory.createZNameList(inameList), expri); //xi:Xi
            writer.addTranslationLine(varDecli.accept(new DeclPrinter(new ZSchTextWriter()))); //[[xi:Xi]]
            i++;
        }

        List<String> lines = new ArrayList<String>();
        String tr = RulesUtils.crossProduct(slvar, ivars, lines);
        for (String l : lines)
            writer.addTranslationLine(l);

        return tr;
    }

    public String visitRefExpr(RefExpr refExpr) {
        String name = refExpr.getZName().toString();

        String tr = "refExpr";
        if (refExpr.getMixfix()) { //Generic Operation Aplication: rel pfun seq ...

            Expr exprl = refExpr.getZExprList().get(0);
            String sleft = exprl.accept(exprPredPrinter);
            String sright = "";
            if ( refExpr.getZExprList().size()>1){
                Expr exprr = refExpr.getZExprList().get(1);
                sright = exprr.accept(exprPredPrinter);
            }

            //Esto no queda igual qeu ExprPredPrinter porque allí se imprimen todas las lineas
            // y se devuelve la nueva var aux sin &;
            // acá es como que la var ya nos viene, entonces no imprimimos la última línea
            //poruqe agregaría un & de más
            List<String> lines = new ArrayList<String>();
            RulesUtils.binRel(slvar, sleft, sright, name, lines);
            StringBuilder trAux = new StringBuilder();
            int i;
            for (i = 0; i< lines.size()-1;i++ )
                trAux.append(writer.line(lines.get(i)));

            tr = trAux.toString() + lines.get(i);

        } else { //Reference o Generic instantiation

            if (refExpr.getZExprList().isEmpty()) {//Reference
                if (ToSlogState.isConstDecl(name)){ //La referencia puede ser un esquema de tipo

                    if( ToSlogState.getExprFromConsDecl(name) instanceof SchExpr){
                        tr = ((SchExpr) ToSlogState.getExprFromConsDecl(name)).accept(this);
                    }
                }
                else{
                    tr = RulesUtils.groundVar(slvar, name);
                }
            }
            //else {}  //generic instantiation ej: \emptyset
        }
        return tr;

    }

    public String visitSchExpr(SchExpr schExpr) {
        ZSchText zSchText = schExpr.getZSchText();
        List<String> ivars;
        ivars = new ArrayList<String>();
        for (Decl decli : zSchText.getZDeclList()) {
            String strSlName="";
            for (Name name : ((VarDecl) decli).getZNameList()){
                strSlName = ToSlogState.getVarSlName(name.toString());
                ivars.add(strSlName);
            }

            String line = (decli.accept(new DeclPrinter(new ZSchTextWriter())));
            if (!ToSlogState.isPrintedSlVar(strSlName)){
                writer.addTranslationLine(line);//[[xi:Xi]]//
                ToSlogState.addPrintedSlVar(strSlName);
            }
        }

        List<String> lines = new ArrayList<String>();
        String tr = RulesUtils.schemaType(slvar, ivars, lines);
        for (String l : lines)
            writer.addTranslationLine(l);

        return tr;
    }

    public String visitSetExpr(SetExpr setExpr) {
        return RulesUtils.groundVar(slvar,setExpr.accept(exprPredPrinter));

    }

    //Puede venir cualquier cosa por ej: x.1 ó (1,2).1
    public String visitTupleSelExpr(TupleSelExpr tupleSelExpr) {
        String crossProduct = tupleSelExpr.getExpr().accept(exprPredPrinter);
        String numeral = tupleSelExpr.getNumeral().toString();
        return RulesUtils.accesToCrossProduct(crossProduct,numeral);
    }

    public String visitPowerExpr(PowerExpr powerExpr) {
        List<String> lines = new ArrayList<String>();
        String tr = RulesUtils.powerSet(slvar, SpecUtils.termToLatex(powerExpr.getExpr()), lines );
        for (String l : lines)
            writer.addTranslationLine(l);
        return tr;

    }

    public String visitApplExpr(ApplExpr applExpr) {
        return RulesUtils.groundVar(slvar,applExpr.accept(exprPredPrinter));
    }

    public String visitBindSelExpr(BindSelExpr bindSelExpr) {
        return RulesUtils.groundVar(slvar,bindSelExpr.accept(exprPredPrinter));
    }
}
