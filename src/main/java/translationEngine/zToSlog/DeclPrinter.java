package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.VarDeclVisitor;
import translationEngine.slogToZ.ToZState;
import translationEngine.zToSlog.writer.Writer;

/**
 * Created by joaquin on 17/06/16.
 */
final class DeclPrinter implements
        VarDeclVisitor<String>{

    private Writer writer;

    DeclPrinter(Writer writer){
        this.writer = writer;
    }

    //Tengo que hacer estos if's porque maxi define reglas que no son regulares
    // para x: \nat -> x in NAT
    // para x: \nat \cross \nat -> x = [x1,x2] & x1 in NAT & x2 in NAT
    // si hubiera sido por ejemplo x in [NAT,NAT] sería otra cosa
    public String visitVarDecl(VarDecl varDecl) {
        Expr expr = varDecl.getExpr();
        ExprDeclPrinter exprDeclPrinter = new ExprDeclPrinter(writer);
        String zvar;
        for (Name var : varDecl.getZNameList()) { // x,y : \num \cross \num
            zvar = var.toString();
            exprDeclPrinter.setSlVar(ToSlogState.getVarSlName(zvar));
            writer.addTranslationLine(expr.accept(exprDeclPrinter));
            ToZState.zVars.put(zvar,null); //SACAR ESTO DE ACÄ
        }
        return writer.getTranslation();
    }
}