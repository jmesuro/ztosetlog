package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ApplExprImpl;
import net.sourceforge.czt.z.visitor.*;
import translationEngine.zToSlog.writer.SetCompWriter;
import translationEngine.zToSlog.writer.Writer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * Created by joaquin on 05/07/16.
 * Visita los Expr a secas, a diferencia de ExprDeclPrinter
 * que es llamado de DeclPrinter
 */
final class ExprPredPrinter implements
        RefExprVisitor<String>,
        TupleSelExprVisitor<String>,
        TupleExprVisitor<String>,
        NumExprVisitor<String>,
        BindSelExprVisitor<String>,
        SetExprVisitor<String>,
        ApplExprVisitor<String>,
        SetCompExprVisitor<String> {

    private Writer writer;

    ExprPredPrinter(Writer writer){
        this.writer = writer;
    }

    public String visitRefExpr(RefExpr refExpr) {
        String tr;
        String sname = refExpr.getZName().toString();

        if (refExpr.getMixfix()) {
            //Generic Operation Aplication: rel pfun seq ...
            tr = sname;
        } else { //Reference o Generic instantiation

            if (refExpr.getZExprList().isEmpty()) {//Reference
                tr = ToSlogState.getSlName(sname);
            } else {//generic instantiation ej: \emptyset \dom
                tr = RulesUtils.isEmptySet(sname) ? RulesUtils.emptySet() : sname;
            }
        }
        return tr;

    }

    //Puede venir cualquier cosa por ej: x.1 ó (1,2).1
    public String visitTupleSelExpr(TupleSelExpr tupleSelExpr) {
        String crossProduct = tupleSelExpr.getExpr().accept(this);
        String numeral = tupleSelExpr.getNumeral().toString();
        return RulesUtils.accesToCrossProduct(crossProduct, numeral);
    }

    public String visitTupleExpr(TupleExpr tupleExpr) {
        ArrayList<String> elems = new ArrayList<String>();
        for (Expr expr : tupleExpr.getZExprList())
            elems.add(expr.accept(this));

        List<String> lines = new ArrayList<String>();
        String var = RulesUtils.tuple(elems, lines);
        for (String l : lines)
            writer.addTranslationLine(l);
        return var;
    }

    public String visitNumExpr(NumExpr numExpr) {
        return numExpr.getZNumeral().toString();
    }

    // X.x1
    public String visitBindSelExpr(BindSelExpr bindSelExpr) {
        String sch = bindSelExpr.getExpr().accept(this);
        String vari = bindSelExpr.getZName().toString();
        return RulesUtils.accesToSchemaType(vari);
    }

    public String visitSetExpr(SetExpr setExpr) {
        ArrayList<String> elems = new ArrayList<String>();
        for (Expr expr : setExpr.getZExprList())
            elems.add(expr.accept(this));

        List<String> lines = new ArrayList<String>();
        String var = RulesUtils.setExtension(elems, lines);
        for (String l : lines)
            writer.addTranslationLine(l);
        return var;
    }

    //imprime el tipo de las variables auxiliares porque no se hacen en el DeclPrinter principal.
   /* private void printAuxVarType(String var){
        Factory factory = new Factory();
        List<Name> lvar  = new ArrayList<Name>();
        Expr expr1 = ToSlogState.getSlExprType(var);

        if (expr1 instanceof RefExpr) {
            String name = ((RefExpr)expr1).getZName().toString();
            if (RulesUtils.isNumericType(name)){
                lvar.add(factory.createZName(var));
                factory.createVarDecl(factory.createZNameList(lvar), expr1).accept(new DeclPrinter(writer));
            }
        }


    }*/

    public String visitApplExpr(ApplExpr applExpr) {

        String auxVar0 = ToSlogState.getAuxVarExprAuxVar(applExpr);
        String tr = "null visitApplExpr";
        if (auxVar0 != null) { // expresion Traducida
            tr = auxVar0;
        }
        else {
            RefExpr refExpr = (RefExpr) applExpr.getLeftExpr();
            Expr expr = applExpr.getRightExpr();

            //Application
            if (!applExpr.getMixfix()) {
                //if (!(expr instanceof TupleExpr)) { // dom R


                    String op = expr.accept(this);
                    String fun = refExpr.accept(this);
                    List<String> lines = new ArrayList<String>();
                    String var = RulesUtils.application(fun, op, lines);
                    for (String l : lines)
                        writer.addTranslationLine(l);
                    tr = var;
                //}


                // Function Operator Application
            } else {

                if (expr instanceof TupleExpr) { // S + T ; H \comp H
                    String opl = ((TupleExpr) expr).getZExprList().get(0).accept(this);
                    String opr = ((TupleExpr) expr).getZExprList().get(1).accept(this);
                    List<String> lines = new ArrayList<String>();
                    String var = RulesUtils.functionOperatorApplication(refExpr.getZName().toString(), opl, opr, lines);
                    for (String l : lines)
                        writer.addTranslationLine(l);
                    tr = var;
                } else if (expr instanceof RefExpr) { // R \inv
                    String op = expr.accept(this);
                    List<String> lines = new ArrayList<String>();
                    String var = RulesUtils.functionOperatorApplication(refExpr.getZName().toString(), op, lines);
                    for (String l : lines)
                        writer.addTranslationLine(l);
                    tr = var;
                } else if (expr instanceof SetExpr) { // \langle 1,2 \rangle
                    ArrayList<String> elems = new ArrayList<String>();
                    for (Expr expri : ((SetExpr) expr).getZExprList()){ // son duplas por ser seq, (1,x1) (2,x2) ...
                        ArrayList<String> tuplei = new ArrayList<String>();
                        tuplei.add(((TupleExpr) expri).getZExprList().get(0).accept(this) );
                        tuplei.add(((TupleExpr) expri).getZExprList().get(1).accept(this) );

                        //elems.add(RulesUtils.tuple(tuplei));
                        List<String> lines = new ArrayList<String>();
                        String nvar = RulesUtils.tuple(tuplei, lines);
                        elems.add(nvar);
                        for (String l : lines)
                            writer.addTranslationLine(l);

                    }

                    List<String> lines = new ArrayList<String>();
                    String var = RulesUtils.setExtension(elems, lines);
                    for (String l : lines)
                        writer.addTranslationLine(l);
                    tr = var;
                } else if (expr instanceof ApplExpr){ // \# (\{ 3 \} \dres relations)
                    String op = expr.accept(this);
                    List<String> lines = new ArrayList<String>();
                    String var = RulesUtils.functionOperatorApplication(refExpr.getZName().toString(), op, lines);
                    for (String l : lines)
                        writer.addTranslationLine(l);
                    tr = var;

                }

            }
            ToSlogState.putExprAuxVar(applExpr,tr);
        }
        return tr;
    }

    /* Esta visita crea dos set de variables, esto es porque las variables de la declaración
    * no van en la tradcción de ris, es decir no van entre corchetes,
    * entonces hago una resta de sets.
    * */
    public String visitSetCompExpr(SetCompExpr setCompExpr) {
        ZSchText zSchText = setCompExpr.getZSchText();
        Writer setCompWriter = new SetCompWriter();
        Set<String> declSlVars = new HashSet<String>(); // variables creadas en la declaración
        Set<String> slVars = new HashSet<String>(); // variables creadas en filtro y pattern
        ToSlogState.pushSlVars(declSlVars);

        //visito Decl
        DeclPrinter dp = new DeclPrinter(setCompWriter);
        String var="";
        for (Decl decl : zSchText.getZDeclList()){
            var = ((VarDecl) decl).getName().toString();
            decl.accept(dp);
        }

        declSlVars = ToSlogState.popSlVarsCreated(); //saco del stack las var de declaración
        ToSlogState.pushSlVars(slVars); // meto en stack el set de las var filtro y pattern

        String decl = setCompWriter.getTranslation();
        setCompWriter.resetTranslation();

        //visito filter
        //String filter = RulesUtils.equality(ToSlogState.getSlName(var), ToSlogState.getSlName(var));
        String filter = null;
        if (zSchText.getPred() != null)
            filter = zSchText.getPred().accept(new PredPrinter(setCompWriter));

        setCompWriter.resetTranslation();

        //visito pattern
        String pattern = ToSlogState.getSlName(var);
        if (setCompExpr.getExpr() != null)
            pattern = (setCompExpr.getExpr().accept(new ExprPredPrinter(setCompWriter)));

        setCompWriter.addTranslationLine(filter);
        filter = setCompWriter.getTranslation();
        setCompWriter.resetTranslation();

        slVars = ToSlogState.popSlVarsCreated();
        slVars.addAll(declSlVars); // resto los conjuntos
        List <String> lslVars = new ArrayList<String>();
        lslVars.addAll(slVars);

        List<String> lines = new ArrayList<String>();
        String tr = RulesUtils.setComp(decl,lslVars,filter,pattern, lines);
        for (String l : lines)
            writer.addTranslationLine(l);

        return tr;
    }
}