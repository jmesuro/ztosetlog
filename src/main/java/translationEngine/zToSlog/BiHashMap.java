package translationEngine.zToSlog;

import java.util.HashMap;

/**
 * Created by jmesuro on 25/11/16.
 */
class BiHashMap<T> extends HashMap<T,T> {

    public T put(T key1, T key2) {
        T removed = remove(key1);
        remove(key2);
        super.put(key1, key2);
        super.put(key2, key1);
        return removed;
    }

    public T remove(Object key) {
        T mapped = super.remove(key);
        super.remove(mapped);
        return mapped;
    }

}
