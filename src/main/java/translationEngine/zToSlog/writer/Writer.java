package translationEngine.zToSlog.writer;

import java.util.List;

/**
 * Created by joaquin on 22/08/16.
 */
public interface Writer {
    String line(String x);
    void addTranslationLine(String line);
    void addTranslationAtom(String atom);
    String getTranslation();
    void resetTranslation();
}
