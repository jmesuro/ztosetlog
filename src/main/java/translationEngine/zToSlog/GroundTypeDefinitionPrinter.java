package translationEngine.zToSlog;

import net.sourceforge.czt.base.ast.Term;
import net.sourceforge.czt.base.visitor.TermVisitor;
import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.visitor.RefExprVisitor;
import translationEngine.zToSlog.writer.Writer;

import java.util.*;

/**
 * Created by joaquin on 19/06/16.
 * Extrae un String con varias lineas, cada una es una definición de tipo ej:
 * si tenemos una declaración
 * \begin{schema}
 * n:\num
 * h:A
 * \end{schema}
 * donde [A], entonces
 * lineas <- int(−2147483648, 2147483647) \n set(A)
 */
final class GroundTypeDefinitionPrinter implements
        RefExprVisitor<String>,
        TermVisitor<String>{

    private Set<String> definedTypes;
    /*private final String NAT = "NAT";
    private final String INT = "INT";
    private final String NAT1 = "NAT1";*/
    private Writer writer;

    GroundTypeDefinitionPrinter(Writer writer){
        this.writer = writer;
        definedTypes = new HashSet<String>();
        writer.addTranslationLine(RulesUtils.NATDEF);
        writer.addTranslationLine(RulesUtils.NAT1DEF);
        writer.addTranslationLine(RulesUtils.INTDEF);
    }

    public void setWriter(Writer writer){
        this.writer = writer;
    }

    public String visitTerm(Term term) {
        Object[] array = term.getChildren();
        for (final Object object : array) {
            if (object instanceof Term) {
                ((Term) object).accept(this);
            }
        }
        return "";
    }

    public String visitRefExpr(RefExpr refExpr) {
        String name = refExpr.getZName().toString();

        /*if (refExpr.getMixfix()) { //Generic Operation Aplication: rel pfun seq ...

            if (RulesUtils.isSeq(refExpr)){
                writer.addTranslationLine(RulesUtils.NATDEF);
                definedTypes.add(NAT);
            }else if (RulesUtils.isSeq1(refExpr)){
                writer.addTranslationLine(RulesUtils.NAT1DEF);
                definedTypes.add(NAT1);
            }

            for (Expr expr : refExpr.getZExprList())
                expr.accept(this);

        }else*/
        {//Reference o Generic instantiation

            /*if (ToSlogState.isNat(name) && !definedTypes.contains(NAT)) {
                writer.addTranslationLine(RulesUtils.NATDEF);
                definedTypes.add(NAT);
            } else if (ToSlogState.isNat1(name) && !definedTypes.contains(NAT1)) {
                writer.addTranslationLine(RulesUtils.NAT1DEF);
                definedTypes.add(NAT1);
            } else if (ToSlogState.isInt(name) && !definedTypes.contains(INT)) {
                writer.addTranslationLine(RulesUtils.INTDEF);
                definedTypes.add(INT);
            } else if (ToSlogState.isBasicType(name) && !definedTypes.contains(name)) {
                writer.addTranslationLine(RulesUtils.basicTypeDefinition(name));
                definedTypes.add(name);
             }*/

            if (ToSlogState.isFreeType(name) && !definedTypes.contains(name)) {
                writer.addTranslationLine(RulesUtils.freeTypeDefinition(name));
                definedTypes.add(name);
            } else if (ToSlogState.isConstDecl(name) && !definedTypes.contains(name)){ // cuando por ejemplo C == A \cross B,
                writer.addTranslationLine(ToSlogState.getExprFromConsDecl(name).accept(this));
            }
        }
        return "";
    }
}