package translationEngine.zToSlog;

import net.sourceforge.czt.z.ast.*;
import net.sourceforge.czt.z.impl.ZFactoryImpl;
import net.sourceforge.czt.z.util.Factory;
import net.sourceforge.czt.z.visitor.*;
import translationEngine.zToSlog.writer.SetCompWriter;
import translationEngine.zToSlog.writer.Writer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jmesuro on 06/05/16.
 * Son predicados que son solo conjunciones de (negación)átomos
 */
final class PredPrinter implements
        MemPredVisitor<String>,
        AndPredVisitor<String>,
        NegPredVisitor<String>,
        OrPredVisitor<String>,
        ImpliesPredVisitor<String>{

    private Writer writer;

    PredPrinter(Writer writer){
        this.writer = writer;
    }


    public String visitAndPred(AndPred andPred) {
        andPred.getLeftPred().accept(this);
        andPred.getRightPred().accept(this);
        return writer.getTranslation();
    }

    public String visitOrPred(OrPred orPred) {
        String r,l,s;
        Writer oldWriter = writer;

        writer = new SetCompWriter();
        orPred.getLeftPred().accept(this);
        l = writer.getTranslation() ;

        writer = new SetCompWriter();
        orPred.getRightPred().accept(this);
        r = writer.getTranslation();


        writer = oldWriter;
        s = RulesUtils.orPred(l,r);
        writer.addTranslationLine(s);

        return  writer.getTranslation();
    }

    public String visitImpliesPred(ImpliesPred impliesPred) {
        ZFactory f = new ZFactoryImpl();
        NegPred np = f.createNegPred(impliesPred.getLeftPred());

        List lp = new ArrayList<Pred>();
        lp.add(np);
        lp.add(impliesPred.getRightPred());
        OrPred op = f.createOrPred(lp);
        op.accept(this);

        return writer.getTranslation();
    }

    public String visitMemPred(MemPred memPred) {
        ExprPredPrinter exprPredPrinter = new ExprPredPrinter(writer);
        Expr leftExpr = memPred.getLeftExpr();
        Expr rightExpr = memPred.getRightExpr();

        //Membership predicate "\in"
        if (!memPred.getMixfix()){
            writer.addTranslationLine((RulesUtils.MembershipPredicate(leftExpr.accept(exprPredPrinter),
                    rightExpr.accept(exprPredPrinter))));
        }


        // Equiality "=" no es un set, es un singletón
        else if (rightExpr instanceof SetExpr) {
            writer.addTranslationLine(RulesUtils.equality(leftExpr.accept(exprPredPrinter),
                    ((SetExpr)rightExpr).getZExprList().get(0).accept(exprPredPrinter)));
        }


        //Other operator applicaton \neq \leq \subseteq \notin
        else {
            //sabemos que la Tupla es una Dupla no ????
            if (leftExpr instanceof TupleExpr){

                String op1 = ((TupleExpr)leftExpr).getZExprList().get(0).accept(exprPredPrinter);
                String op2 = ((TupleExpr)leftExpr).getZExprList().get(1).accept(exprPredPrinter);

                writer.addTranslationLine(RulesUtils.
                        otherOperatorApplication(((RefExpr)rightExpr).getZName().toString(),op1,op2));

            }

        }
        return writer.getTranslation();
    }

    public String visitNegPred(NegPred negPred) {
        Pred pred = negPred.getPred();
        Factory f = new Factory();
        String tr = "negPred";

        if (pred instanceof MemPred){
            MemPred memPred = (MemPred) pred;
            Expr rightExpr = memPred.getRightExpr();
            Expr leftExpr = memPred.getLeftExpr();

            if (!memPred.getMixfix()){ // era Memership Predicate "in"

                RefExpr refExpr = f.createRefExpr(f.createZName(" _ ∉ _ "));
                TupleExpr tupleExpr = f.createTupleExpr(leftExpr,rightExpr);
                List<Expr> list = new ArrayList<Expr>();
                list.add(tupleExpr);
                list.add(refExpr);

                tr = f.createMemPred(list,true).accept(this);
            }

            else if (rightExpr instanceof SetExpr){ // era Equality " _ = _ "

                    RefExpr refExpr = f.createRefExpr(f.createZName(" _ ≠ _ "));
                    TupleExpr tupleExpr = f.createTupleExpr(leftExpr,
                            ((SetExpr)rightExpr).getZExprList().get(0));

                    List<Expr> list = new ArrayList<Expr>();
                    list.add(tupleExpr);
                    list.add(refExpr);

                    tr = f.createMemPred(list,true).accept(this);
            }else {
                if (leftExpr instanceof TupleExpr) {

                    Expr el = ((TupleExpr)leftExpr).getZExprList().get(0);
                    Expr er = ((TupleExpr)leftExpr).getZExprList().get(1);

                    List<Expr> list = new ArrayList<Expr>();
                    String op = ((RefExpr) rightExpr).getZName().toString();

                    if (op.equals(" _ ≠ _ ")) {

                        List<Expr> setl = new ArrayList<Expr>();
                        setl.add(er);

                        SetExpr setExpr = f.createSetExpr(f.createZExprList(setl));

                        list.add(el);
                        list.add(setExpr);

                        tr = f.createMemPred(list,true).accept(this);

                    }else if(op.equals(" _ ∉ _ ")){
                        list.add(el);
                        list.add(er);
                        tr = f.createMemPred(list,false).accept(this);
                    }else {
                        // no se arma la opción negada porque no es reversible esta negación
                        ExprPredPrinter exprPredPrinter = new ExprPredPrinter(writer);
                        String op1 = ((TupleExpr)leftExpr).getZExprList().get(0).accept(exprPredPrinter);
                        String op2 = ((TupleExpr)leftExpr).getZExprList().get(1).accept(exprPredPrinter);

                        writer.addTranslationLine(RulesUtils.
                                otherOperatorApplicationNegated(((RefExpr)rightExpr).getZName().toString(),op1,op2));

                    }
                }
            }
        }
        return tr;
    }



}
