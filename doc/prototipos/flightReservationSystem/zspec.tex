%read "/home/mcristia/escritos/sets2014/prototipos/flightReservationSystem/zspec.tex";

\documentclass{article}
\usepackage{z-eves}
\usepackage{framed}

\newcommand{\setlog}{$\{log\}$}

\newcommand{\desig}[2]{\item #1 $\approx #2$}

\newenvironment{designations}
  {\begin{leftbar}
    \begin{list}{}{\setlength{\labelsep}{0cm}
                   \setlength{\labelwidth}{0cm}
                   \setlength{\listparindent}{0cm}
                   \setlength{\rightmargin}{\leftmargin}}}
  {\end{list}\end{leftbar}}

\title{Z Specification of a Simple Flight Reservation System}

\author{Maximiliano Cristi\'a \and Gianfranco Rossi}

\begin{document}
\maketitle

This document describes the Z specification of a simple flight reservation system. This specification is used as a case study for a possible prototype generation tool based on Z specifications and Prolog+\setlog+XPCE code.

\section{Designations}

\begin{designations}
\desig{$y$ is a leap year}{y \in leap}
\desig{Day $d$, month $m$ and year $y$ configure a valid date}{(d,m,y) \in validDate}
\desig{$f$ is a flight number}{f \in FNUM}
\desig{$c$ is a city}{c \in CITY}
\desig{The user can select flight origin, destination and date}{sodd}
\desig{The user can select a flight number}{sf}
\desig{The user can select flight seat}{ss}
\desig{The system prints the flight data}{pinfo}
\desig{Users cannot reserve flights}{disabled}
\desig{Flight number $f$ flights from origin $o$ to destination $d$}{trip~f = (o,d)}
\desig{Flight number $f$ flights at time $t$}{time~f = t}
\desig{$d$ is a date in which flight number $f$ flights}{f \mapsto d \in dates}
\desig{Flight number $f$ has $n$ seats for passengers}{seats~f = n}
\desig{$n$ is an available seat (number) in flight number $f$}{available~f = n}
\desig{The user wants to flight from city}{uOrig}
\desig{The user wants to flight to city}{uDest}
\desig{The user wants to flight in flight number}{uFnum}
\desig{The user wants to flight on seat number}{uSeat}
\end{designations}

\section{Basic Definitions}

\begin{zed}
TIME == \nat \cross \nat \also
DATE == \nat \cross \nat \cross \nat \also
\end{zed}

\begin{axdef}
leap: \power \nat
\where
leap = \{y:\nat | y \mod 400 = 0 \lor y \mod 100 \neq 0 \land y \mod 4 = 0\}
\end{axdef}

\begin{axdef}
validDate: \power DATE
\where
validDate = \\
  \t1 \{d:DATE | \\
    \t2 (d.2 \in \{1,3,5,7,8,10,12\} \land d.1 \in 1 \upto 31 \\
    \t2 \lor d.2 \in \{4,6,9,11\} \land d.1 \in 1 \upto 30 \\
    \t2 \lor d.2 = 2 \land (d.1 \in 1 \upto 28 \lor (d.3 \in leap \land d.1 \in 1 \upto 29)))\}
\end{axdef}

\begin{axdef}
validTime: \power TIME
\where
validTime = \{t:TIME | t.1 \in 0 \upto 23 \land t.2 \in 0 \upto 59\}
\end{axdef}

\begin{zed}
[FNUM, CITY] \also
WFS ::= sodd | sf | ss | pinfo | disabled \also
MSG ::= \\
  \t1 ok  \\
  \t1 | noFlights \\
  \t1 | notADate \\
  \t1 | noFlightsInDate \\
  \t1 | wrongFlightNumber \\
  \t1 | noSeatsAvailable \\
  \t1 | wrongSeat \\
  \t1 | cannotDisable \\
  \t1 | cannotEnable \\
  \t1 | flightNumberAlreadyInUse \\
  \t1 | origEqualsDest \\
  \t1 | notATime | tooFewSeats | flightDoesNotExist | dateAlreadyExists \also
\end{zed}

\section{State Space}

\begin{schema}{FlightsData}
flights: FNUM \pfun CITY \cross CITY \\
times: FNUM \pfun TIME \\
dates: FNUM \rel DATE \\
seats: FNUM \pfun \nat \\
\end{schema}

\begin{schema}{FlightsSeats}
available: FNUM \pfun \power \nat
\end{schema}

\begin{schema}{UserChoices}
uOrig,uDest: CITY \\
uDate: DATE \\
uFnum: FNUM \\
uSeat: \nat
\end{schema}

\begin{zed}
WSStep \defs [st:WFS] \also
FRSystem \defs FlightsData \land FlightsSeats \land UserChoices \land WSStep
\end{zed}

\section{Initial State}
\begin{schema}{FlightsDataInit}
FlightsData
\where
flights = \emptyset \\
times = \emptyset \\
dates = \emptyset \\
seats = \emptyset \\
\end{schema}

\begin{zed}
FlightsSeatsInit \defs [FlightsSeats | available = \emptyset] \also
WSStepInit \defs [WSStep | st = sodd] \also
\end{zed}

\section{State Invariants}

\begin{schema}{FlightsDataInv}
FlightsData
\where
\dom flights = \dom times = \dom dates = \dom seats \\
\ran times \subseteq validTime \\
\ran dates \subseteq validDate
\end{schema}

\begin{schema}{FlightsSeatsInv}
FlightsSeats; FlightsData
\where
\dom available = \dom seats \\
\forall f:FNUM | f \in \dom available @ available~f \subseteq 1 \upto seats~f
\end{schema}


\section{Operations}

\subsection{Disable flight reservations}
An administrative officer of the airline disables flight reservation to add more flights to the database.

\begin{schema}{DisableOk}
\Xi UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
m!:MSG
\where
st = sodd \\
st' = disabled \\
m! = ok
\end{schema}

\begin{zed}
DisableE \defs [\Xi FRSystem; m!:MSG | st \neq sodd \land m! = cannotDisable] \also
Disable \defs DisableOk \lor DisableE
\end{zed}

\subsection{Enable flight reservations}
An administrative officer of the airline enables flight reservations.

\begin{schema}{EnableOk}
\Xi UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
m!:MSG
\where
st = disabled \\
st' = sodd \\
m! = ok
\end{schema}

\begin{zed}
EnableE \defs [\Xi FRSystem; m!:MSG | st \neq disabled \land m! = cannotEnable] \also
Enable \defs EnableOk \lor EnableE
\end{zed}

\subsection{Add a flight to the database}

When flight reservation is disabled administrative employees can add flights to the database.

\begin{schema}{AddFlightOk}
\Xi UserChoices; \Xi WSStep; \Delta FlightsSeats; \Delta FlightsData \\
f?:FNUM \\
orig?,dest?:CITY \\
date?: DATE \\
time?: TIME \\
s?: \nat \\
m!:MSG
\where
st = disabled \\
f? \notin \dom flights \\
orig? \neq dest? \\
date? \in validDate \\
time? \in validTime \\
s? > 10 \\
flights' = flights \cup \{f? \mapsto (orig?,dest?)\} \\
times' = times \cup \{f? \mapsto time?\} \\
dates' = dates \cup \{f? \mapsto date?\} \\
seats' = seats \cup \{f? \mapsto s?\} \\
available' = available \cup \{f? \mapsto 1 \upto s?\} \\
m! = ok
\end{schema}

\begin{schema}{FlightNumberAlreadyInUse}
\Xi FRSystem \\
f?:FNUM \\
m!: MSG
\where
f? \in \dom flights \\
m! = flightNumberAlreadyInUse
\end{schema}

\begin{schema}{OrigEqualsDest}
\Xi FRSystem \\
orig?,dest?:CITY \\
m!:MSG
\where
orig? = dest? \\
m! = origEqualsDest
\end{schema}

\begin{schema}{WrongDate}
\Xi FRSystem \\
date?: DATE \\
m!: MSG
\where
date? \notin validDate \\
m! = notADate
\end{schema}

\begin{schema}{WrongTime}
\Xi FRSystem \\
time?: TIME \\
m!: MSG
\where
time? \notin validTime \\
m! = notATime
\end{schema}

\begin{schema}{TooFewSeats}
\Xi FRSystem \\
s?: \nat \\
m!: MSG
\where
s? \leq 10 \\
m! = tooFewSeats
\end{schema}

\begin{zed}
AddFlight \defs \\
  \t1 AddFlightOk \\
  \t1 \lor FlightNumberAlreadyInUse \\
  \t1 \lor OrigEqualsDest \lor WrongDate \lor WrongTime \lor TooFewSeats
\end{zed}

\subsection{Add a date to a flight}

When flight reservation is disabled administrative employees can add dates to existing flights.

\begin{schema}{AddDateOk}
\Xi UserChoices; \Xi WSStep; \Xi FlightsSeats; \Delta FlightsData \\
f?:FNUM \\
date?: DATE \\
m!:MSG
\where
st = disabled \\
f? \in \dom flights \\
date? \in validDate \\
f? \mapsto date? \notin dates \\
dates' = dates \cup \{f? \mapsto date?\} \\
m! = ok \\
flights' = flights \\
times' = times \\
seats' = seats
\end{schema}

\begin{schema}{FlightDoesNotExist}
\Xi FRSystem \\
f?: FNUM \\
m!: MSG
\where
f? \notin \dom flights \\
m! = flightDoesNotExist
\end{schema}

\begin{schema}{DateAlreadyExists}
\Xi FRSystem \\
f?: FNUM \\
date?:DATE \\
m!: MSG
\where
f? \mapsto date? \in dates \\
m! = dateAlreadyExists
\end{schema}

\begin{zed}
AddDate \defs AddDateOk \lor FlightDoesNotExist \lor WrongDate \lor DateAlreadyExists
\end{zed}


\subsection{The user selects origin, destination and date}

\begin{schema}{SelectODDOk}
\Delta UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
orig?,dest?:CITY \\
date?: DATE \\
m!: MSG
\where
st = sodd \\
date? \in validDate \\
orig? \mapsto dest? \in \ran flights \\
uOrig' = orig? \\
uDest' = dest? \\
uDate' = date? \\
m! = ok \\
st' = sf \\
uFnum' = uFnum \\
uSeat' = uSeat
\end{schema}

\begin{schema}{NoFlightsBetween}
\Xi FRSystem \\
orig?,dest?:CITY \\
m!: MSG
\where
orig? \mapsto dest? \notin \ran flights \\
m! = noFlights
\end{schema}

\begin{zed}
SelectODD \defs SelectODDOk \lor WrongDate \lor NoFlightsBetween
\end{zed}

\subsection{The user selects a flight}
\begin{schema}{SelectFNOk}
\Delta UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
f?:FNUM \\
m!: MSG
\where
st = sf \\
\dom(flights \rres \{uOrig \mapsto uDest\}) \cap \dom (dates \rres \{uDate\}) \neq \emptyset \\
f? \in \dom(flights \rres \{uOrig \mapsto uDest\}) \cap \dom (dates \rres \{uDate\}) \\
uFnum' = f? \\
m! = ok \\
st' = ss \\
uOrig' = uOrig \\
uDest' = uDest \\
uDate' = uDate \\
uSeat' = uSeat
\end{schema}

\begin{schema}{NoFlightsInDate}
\Delta WSStep; \Xi UserChoices; \Xi FlightsSeats; \Xi FlightsData \\
m!: MSG
\where
\dom(flights \rres \{uOrig \mapsto uDest\}) \cap \dom (dates \rres \{uDate\}) = \emptyset \\
st' = sodd \\
m! = noFlightsInDate \\
\end{schema}

\begin{schema}{WrongFlightNumber}
\Xi FRSystem  \\
f?:FNUM \\
m!: MSG
\where
\dom(flights \rres \{uOrig \mapsto uDest\}) \cap \dom (dates \rres \{uDate\}) \neq \emptyset \\
f? \notin \dom(flights \rres \{uOrig \mapsto uDest\}) \cap \dom (dates \rres \{uDate\}) \\
m! = wrongFlightNumber \\
\end{schema}

\begin{zed}
SelectFN \defs SelectFNOk \lor NoFlightsInDate \lor WrongFlightNumber
\end{zed}


\subsection{The user selects a seat}
\begin{schema}{SelectSNOk}
\Delta UserChoices; \Delta WSStep; \Delta FlightsSeats; \Xi FlightsData \\
s?:\nat \\
m!: MSG
\where
st = ss \\
available~~uFnum \neq \emptyset \\
s? \in available~~uFnum \\
uSeat' = s? \\
available' = available \oplus \{uFnum \mapsto (available~~uFnum) \setminus \{s?\}\} \\
m! = ok \\
st' = pinfo \\
uOrig' = uOrig \\
uDest' = uDest \\
uDate' = uDate \\
uFnum' = uFnum \\
\end{schema}

\begin{schema}{NoSeats}
\Xi UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
m!: MSG
\where
available~uFnum = \emptyset \\
m! = noSeatsAvailable \\
st' = sf \\
\end{schema}

\begin{schema}{WrongSeat}
\Xi FRSystem \\
s?:\nat \\
m!: MSG
\where
available~~uFnum \neq \emptyset \\
s? \notin available~~uFnum \\
m! = wrongSeat \\
\end{schema}

\begin{zed}
SelectSN \defs SelectSNOk \lor NoSeats \lor WrongSeat
\end{zed}

\subsection{The user receives flight information}
\begin{schema}{PrintFI}
\Xi UserChoices; \Delta WSStep; \Xi FlightsSeats; \Xi FlightsData \\
orig!, dest!: CITY \\
date!:DATE \\
time!:TIME \\
f!: FNUM \\
s!:\nat \\
m!: MSG
\where
st = pinfo \\
orig! = uOrig \\
dest! = uDest \\
date! = uDate \\
time! = times~~uFnum \\
f! = uFnum \\
s! = uSeat \\
m! = ok \\
st' = sodd \\
\end{schema}

\end{document}

