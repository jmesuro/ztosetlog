% Si un esquema ya se tradujo, no hay que
% traducirlo otra vez. Por ejemplo WrongDate
% se usa en varias operaciones
disable :-
  disableOk('Disable')
  ;
  disableE('Disable').

disableOk(Tn) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = sodd &
          StNew = disabled &
          M = 'ok' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

disableE(Tn) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St neq diabled & 
          M = 'cannotDisable' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

enable :-
  enableOk('Enable')
  ;
  enableE('Enable').

enableOk(Tn) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = disabled &
          StNew = sodd &
          M = 'ok' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

enableE(Tn) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St neq diabled & 
          M = 'cannotEnable' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).


addFlight(F,Orig,Dest,Date,Time,S) :-
  addFlightOk('AddFlight',F,Orig,Dest,Date,Time,S)
  ;
  flightNumberAlreadyInUse('AddFlight',F)
  ;
  origEqualsDest('AddFlight',Orig,Dest)
  ;
  wrongDate('AddFlight',Date)
  ;
  wrongTime('AddFlight',Time)
  ;
  tooFewSeats('AddFlight',S).

addFlightOk(Tn,F,Orig,Dest,Date,Time,S) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = disabled &
          dom(Flight,DF) &
          F nin DF &
          Orig neq Dest &
          validDate(Date) &
          validTime(Time)  &
          S > 10 &
          un(Flights,{[F,[Orig,Dest]]},FlightsNew) &
          un(Times,{[F,Time]},TimesNew) &
          un(Dates,{[F,Date]},DatesNew) &
          un(Seats,{[F,S]},SeatsNew) &
          un(Available,{[F,int(1,S)]},AvailableNew) &
          M = 'ok' &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

flightNumberAlreadyInUse(Tn,F) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( dom(Flights,DF) & 
          F in DF & 
          M = 'flightNumberAlreadyInUse' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

origEqualsDest(Tn,Orig,Dest) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( Orig = Dest & 
          M = 'origEqualsDest'),
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

wrongDate(Tn,Date) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( not(validDate(Date)) & 
          M = 'notADate' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

wrongTime(Tn,Time) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( not(validTime(Time)) & 
          M = 'notATime' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

tooFewSeats(Tn,S) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( SE =< 10 & 
          M = 'tooFewSeats' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).


addDate(F,Date) :-
  addDateOk('AddDate',F,Date)
  ;
  flightDoesNotExist('AddDate',F)
  ;
  dateAlreadyExists('AddDate',F,Date)
  ;
  wrongDate('AddDate',Date).

addDateOk(Tn,F,Date) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = disabled &
          dom(Flights,DF) &
          F in DF &
          validDate(Date) &
          [F,Date] nin Dates &
          un(Dates,{[F,Date]},DatesNew) &
          M = 'ok' &
          Flights = FlightsNew &
          Times = TimesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

flightDoesNotExist(Tn,F) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( dom(Flights,DF) & 
          F nin DF & 
          M = 'flightDoesNotExist'),
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

dateAlreadyExists(Tn,F,Date) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( [F,Date] in Dates & 
          M = 'dateAlreadyExists' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

selectODD(Orig,Dest,Date) :-
  selectODDOk('SelectODD',Orig,Dest,Date)
  ;
  wrongDate('SelectODD',Date)
  ;
  noFlightsBetween('SelectODD',Orig,Dest).

selectODDOk(Tn,Orig,Dest,Date) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = sodd &
          validDate(Date) &
          ran(Flights,RF) &
          [Orig,Dest] in RF &
          UOrigNew = Or &
          UDestNew = De &
          UDateNew = Date &
          StNew = sf &
          M = 'ok'),
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

noFlightsBetween(Tn,Orig,Dest) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( ran(Flights,RF) & 
          [Orig,Dest] nin RF & 
          M = 'noFlights' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

selectFN(F) :-
  selectFNOk('SelectFN',F)
  ;
  noFlightsInDate('SelectFN',F)
  ;
  wrongFlightNumber('SelectFN',F).

selectFNOk(Tn,F) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = sf &
          rres({[UOrig,UDest]},Flights,F1) &
          dom(F1,DF1) &
          rres({UDate},D,D1) &
          dom(D1,DD1) &
          inters(DF1,DD1,In) &
          In neq {} &
          F in In &
          UFnumNew = F &
          StNew = ss &
          M = 'ok' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

noFlightsInDate(Tn,F) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( rres({[UOrig,UDest]},Flights,F1) &
          dom(F1,DF1) &
          rres({UDate},D,D1) &
          dom(D1,DD1) &
          inters(DF1,DD1,In) &
          In = {} &
          StNew = sodd &
          M = 'noFlightsInDate' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

wrongFlightNumber(Tn,F) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( rres({[UOrig,UDest]},Flights,F1) &
          dom(F1,DF1) &
          rres({UDate},D,D1) &
          dom(D1,DD1) &
          inters(DF1,DD1,In) &
          In neq {} &
          F nin In &
          M = 'wrongFlightNumber'),
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).


selectSN(S) :-
  selectSNOk('SelectSN',S)
  ;
  noSeats('SelectSN')
  ;
  wrongSeat('SelectSN',S).

selectSNOk(Tn,S) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = ss &
          apply(Available,UFnum,As) &
          As neq {} &
          S in As &
          USeatNew = Sn &
          diff(As,{S},S1) &
          oplus(Available,{[UFnum,S1]},AvailableNew) &
          StNew = pinfo &
          M = 'ok' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

noSeats(Tn) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( apply(Available,UFnum,As) &
          As = {} &
          StNew = sf &
          M = 'noSeatsAvailable' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

wrongSeat(Tn,S) :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( apply(Available,UFnum,As) &
          As neq {} &
          S nin As &
          M = 'wrongSeat' &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew &
          St = StNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(m,M).

printFI :-
  nb_getval(flights,Flights),
  nb_getval(times,Times),
  nb_getval(dates,Dates),
  nb_getval(seats,Seats),
  nb_getval(available,Available),
  nb_getval(uOrig,UOrig),
  nb_getval(uDest,UDest),
  nb_getval(uDate,UDate),
  nb_getval(uFnum,UFnum),
  nb_getval(uSeat,USeat),
  nb_getval(st,St),
  setlog( St = pinfo &
          Orig = UOrig &
          Dest = UDest &
          Date = UDate &
          apply(Times,UFnum,Time) &
          F = UFnum &
          S = USeat &
          M = 'ok' &
          StNew = sodd &
          Flights = FlightsNew &
          Times = TimesNew &
          Dates = DatesNew &
          Seats = SeatsNew &
          Available = AvailableNew &
          UOrig = UOrigNew &
          UDest = UDestNew &
          UDate = UDateNew &
          UFnum = UFnumNew &
          USeat = USeatNew),
  saveFile(Tn,
           Flights,
           Times,
           Dates,
           Seats,Available,UOrig,UDest,UDate,UFnum,USeat,StNew),
  nb_getval(view,V),
  updateState(V),
  nb_getval(flights,FlightsNew),
  nb_getval(times,TimesNew),
  nb_getval(dates,DatesNew),
  nb_getval(seats,SeatsNew),
  nb_getval(available,AvailableNew),
  nb_getval(uOrig,UOrigNew),
  nb_getval(uDest,UDestNew),
  nb_getval(uDate,UDateNew),
  nb_getval(uFnum,UFnumNew),
  nb_getval(uSeat,USeatNew),
  nb_setval(st,StNew),
  nb_getval(orig,Orig),
  nb_getval(dest,Dest),
  nb_getval(date,Date),
  nb_getval(time,Time),
  nb_getval(f,F),
  nb_getval(s,S),
  nb_getval(m,M).


